package com.isuisudmt.cashoutReport;

import android.util.Base64;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;


public class CashoutReportPresenter implements CashoutReportContract.UserActionsListener {

    /**
     * Initialize ReportContractView
     */
    private CashoutReportContract.View microReportContractView;
    private AEPSAPIService aepsapiService;
    private ArrayList<CashoutReportModel> microReportModelArrayList ;
    /**
     * Initialize ReportPresenter
     */
    public CashoutReportPresenter(CashoutReportContract.View microReportContractView) {
        this.microReportContractView = microReportContractView;
    }

    @Override
    public void loadReports(final String fromDate, final String toDate, final String token, final String transactionType) {
        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
            microReportContractView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                           // if(transactionType.equalsIgnoreCase("WALLETCASHOUT")){
                                encryptedReport(fromDate,toDate,token,transactionType,encodedUrl);
                            /*}else{
                                encryptedReport(fromDate,toDate,token,transactionType,"https://uatapps.iserveu.online/STAGEING/BQ/transactiondetails");
                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


        } else {
            microReportContractView.emptyDates();
        }
    }



    public void encryptedReport(String fromDate, String toDate, String token,String type,String encodedUrl){
       /*final ReportAPI reportAPI =
           this.aepsapiService.getClient().create(ReportAPI.class);

       Call<ReportResponse> call = reportAPI.insertUser(token,new ReportRequest(fromDate,toDate,"AEPS"),encodedUrl);
*/
        //   "transactionType":"AEPS","fromDate":"2020-03-06","toDate":"2020-03-07"
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType",type);
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate",toDate);

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                // Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                                JSONArray jsonArray = obj.getJSONArray("BQReport");

                                CashoutReportResponse reportResponse =new CashoutReportResponse() ;
                                ArrayList<CashoutReportModel> reportModels = new ArrayList<>();
                                for(int i =0 ; i<jsonArray.length();i++){
                                    CashoutReportModel reportModel = new CashoutReportModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    reportModel.setId(jsonObject1.getString("Id"));

                                    String previousAmount = jsonObject1.getString("previousAmount");
                                    if(!previousAmount.equalsIgnoreCase("null")){
                                        reportModel.setPreviousAmount(Double.valueOf(previousAmount));
                                    }else{
                                        reportModel.setPreviousAmount(0.0);
                                    }

                                    String balanceAmount = jsonObject1.getString("balanceAmount");
                                    if(!balanceAmount.equalsIgnoreCase("null")){
                                        reportModel.setBalanceAmount(Double.valueOf(balanceAmount));
                                    }else{
                                        reportModel.setBalanceAmount(0.0);
                                    }

                                    reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                    reportModel.setApiTId(jsonObject1.getString("apiTid"));
                                    reportModel.setStatus(jsonObject1.getString("status"));
                                    reportModel.setTransactionMode("");
                                    reportModel.setOperationPerformed(jsonObject1.getString("toAccount"));
                                    /*reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setBankName(jsonObject1.getString("bankName"));
*/

                                    reportModel.setUserName(jsonObject1.getString("userName"));
                                    reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                    reportModel.setMasterName(jsonObject1.getString("masterName"));
                                    reportModel.setUserTrackId("");
                                    reportModel.setCardDetail("");
                                    reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                    reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                   // reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                    reportModels.add(reportModel);
                                }
                                Collections.reverse(reportModels);
                                reportResponse.setmATMTransactionReport(reportModels);


                                // Gson gson = new Gson();
                                // reportResponse = gson.fromJson(jsonArray.toString(),ReportResponse.class);

                                /*if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                                    ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                                    double totalAmount = 0;
                                    for (int i = 0; i < result.size(); i++) {
                                        totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                                    }
                                    reportView.reportsReady(result, String.valueOf(totalAmount));
                                }*/


                                if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                                    ArrayList<CashoutReportModel> result = reportResponse.getmATMTransactionReport();
                                    double totalAmount = 0;
                                    for(int i = 0; i<result.size(); i++) {
                                        totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                                    }
                                    microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                                }
                                microReportContractView.hideLoader();
                                microReportContractView.showReports();
                                //  System.out.println(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                microReportContractView.hideLoader();
                                microReportContractView.showReports();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            microReportContractView.hideLoader();
                            microReportContractView.showReports();
                        }
                    });








        }catch (Exception e){
            e.printStackTrace();
        }









       /*call.enqueue(new Callback<ReportResponse>() {
           @Override
           public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                *//*response.body(); // have your all data
                String userName = response.body().getStatus();*//*
               if(response.isSuccessful()) {

                   ReportResponse reportResponse = response.body();
                   Log.v("Laxmi","hfh"+reportResponse);

                   if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                       ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                       double totalAmount = 0;
                       for(int i = 0; i<result.size(); i++) {
                           totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                       }
                       reportView.reportsReady(result, String.valueOf(totalAmount));
                   }
               }
               reportView.hideLoader();
               reportView.showReports();
           }

           @Override
           public void onFailure(Call<ReportResponse> call, Throwable t) {

               reportView.hideLoader();
               reportView.showReports();

           }
       });*/
    }







   /* public void encyptedMicroReport(String fromDate, String toDate, String token, String transactionType, String encodedUrl){
        MicroReportApi reportAPI =
            this.aepsapiService.getClient().create(MicroReportApi.class);

        Call<MicroReportResponse> call = reportAPI.insertUser(token,new MicroReportRequest(fromDate,toDate,transactionType),encodedUrl);

        call.enqueue(new Callback<MicroReportResponse>() {
            @Override
            public void onResponse(Call<MicroReportResponse> call, Response<MicroReportResponse> response) {
                if(response.isSuccessful()) {

                    MicroReportResponse reportResponse = response.body();
                    if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                        ArrayList<MicroReportModel> result = reportResponse.getmATMTransactionReport();
                        double totalAmount = 0;
                        for(int i = 0; i<result.size(); i++) {
                            totalAmount += Double.parseDouble(result.get(i).getAmountTransacted());
                        }
                        microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                    }
                }
                microReportContractView.hideLoader();
                microReportContractView.showReports();
            }

            @Override
            public void onFailure(Call<MicroReportResponse> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.showReports();
            }
        });
    }
*/
    @Override
    public void refreshReports(final String token, final String amount,final String transactionType, final String transactionMode,  final String clientUniqueId) {
        microReportContractView.showLoader();
        if(amount == null || amount.matches("")){
            microReportContractView.checkAmount("1");
            return;
        }
        if(transactionMode ==null || transactionMode.matches("")){
            microReportContractView.checkTransactionMode("1");
            return;
        }
        if(transactionType == null || transactionType.matches("")){
            microReportContractView.checkTransactionType("1");
            return;
        }
        if(clientUniqueId == null || clientUniqueId.matches("")){
            microReportContractView.checkClientId("1");
            return;
        }

      /*  AndroidNetworking.get(GET_MICRO_ATM_REPORT_URL)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject obj = new JSONObject(response.toString());
                        String key = obj.getString("hello");
                        System.out.println(">>>>-----"+key);
                        byte[] data = Base64.decode(key,Base64.DEFAULT);
                        String encodedUrl = new String(data, "UTF-8");

                        encryptLoadReport(token,amount,transactionType,transactionMode,clientUniqueId,encodedUrl);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onError(ANError anError) {

                }
            });*/

    }


    /**
     *  load Reports of  ReportActivity
     */

/*
    public void encryptLoadReport(String token, final String amount, final String transactionType, final String transactionMode, final String clientUniqueId,String encodedUrl){
        RefreshApi reportAPI = this.aepsapiService.getClient().create(RefreshApi.class);

        Call<RefreshModel> call = reportAPI.insertUser(token,new RefreshRequest(amount,transactionType,transactionMode,clientUniqueId),encodedUrl);

        call.enqueue(new Callback<RefreshModel>() {
            @Override
            public void onResponse(Call<RefreshModel> call, Response<RefreshModel> response) {

                if(response.isSuccessful()){
                    RefreshModel refreshModel = response.body();
                    if (refreshModel != null) {
                        microReportContractView.hideLoader();
                        microReportContractView.refreshDone(refreshModel);
                    }
                }else{
                    microReportContractView.hideLoader();
                    microReportContractView.emptyRefreshData("1");
                }
            }

            @Override
            public void onFailure(Call<RefreshModel> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.emptyRefreshData("1");
            }
        });
    }
*/

}
