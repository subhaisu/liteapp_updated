package com.isuisudmt.login;

public class LoginContract {
    public interface View {
        void fetchedV1Response(boolean status, String response);
    }

    public interface UserInteraction{
        void getV1Response(String base_url);
    }
}
