package com.isuisudmt.dmt;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.isuisudmt.CommonUtil;
import com.isuisudmt.Constants;
import com.isuisudmt.R;
import com.isuisudmt.SelectCashoutOptionActivity;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bluetooth.BluetoothPrinter;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.settings.MainActivity;
import com.isuisudmt.utils.APIFundTransferService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.isuisudmt.BuildConfig.ADD_AND_PAY;
import static com.isuisudmt.BuildConfig.ADD_BENE_URL;
import static com.isuisudmt.BuildConfig.ADD_CUSTOMER;
import static com.isuisudmt.BuildConfig.BULK_TRANSFER;
import static com.isuisudmt.BuildConfig.BULK_TRANSFER_WALLET3;
import static com.isuisudmt.BuildConfig.DELETE_BENE_DMT;
import static com.isuisudmt.BuildConfig.RESEND_OTP;
import static com.isuisudmt.BuildConfig.TRANSACTION;
import static com.isuisudmt.BuildConfig.VERIFY_BENE_URL;
import static com.isuisudmt.BuildConfig.VERIFY_OTP;
import static com.isuisudmt.BuildConfig.WALLET2_TRANSACTION_API;
import static com.isuisudmt.BuildConfig.WALLET3_TRANSACTION_API;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransferFragment extends AppCompatActivity implements FundTransferContract.View {

    CoordinatorLayout coordinatorLayout;

    ImageView imageView, addbene, view_bene_list, show_bene_list;
    LinearLayout pay_add, wallet_info, add_verify, single_bene, bank, loadmore, wallet_content_one, wallet_content_two, wallet_content_three, main_container, new_customer, verify_number, add_new_bene, single_bene_bank, bene_ll;
    Button load_more, enroll_number, resend_otp, verify, create_add_bene, verify_bene, addAndPayBtn, submit_pay;
    EditText mobile_number, amount_transfer, ifc_code, customer_name, otp;
    EditText acc_no, ifccode, bene_name;
    RecyclerView benelistData;
    TextView bank_name_autocomplete;
    SessionManager session;
    TextView add_bene, bankname, wallet_balance_one, wallet_balance_two, wallet_balance_three, wallet_one, wallet_two, wallet_three, amount_to_transfer;
    String _token;
    int wallet_selected = 1;
    String pipeNo = "3";//Default for wallet 3
    String beneId, customerId, _amount_transfer, uid = "fojoo", transactionMode = "NEFT";

    ProgressBar progressBar, progressBar_main;
    private ArrayList<String> listOfBank = new ArrayList<>();
    final HashMap<String, List<BankListModel>> bankDataMap = new HashMap<>();
    ArrayList<BankListModel> bankDetailList = new ArrayList<>();
    String selectedBank = "", getAccountNo = "";
    Set<String > bank_listSet=new HashSet<>();

    BeneRecycleAdapter bebeAdapter;
    ArrayList<BeneModel> beneList = new ArrayList<BeneModel>();
    Button show_recept;
    TextView selectedBeneName, amouttxt, select_bankName, select_ifccode;
    ProgressBar progress_bene;
    RadioGroup rg_add_bene, rg_select_bene;
    RadioButton rb_select_neft, rb_select_imps, rb_add_imps, rb_add_neft;

    Boolean walletInfoStatus = true;

    ArrayList<String> paymentMode = new ArrayList<String>();
    ArrayList<String> paymentMode1 = new ArrayList<String>();
    ArrayList<String> paymentMode2 = new ArrayList<String>();

    private FundTransferPresenter mActionsListener;

    private APIFundTransferService apiFundTransferService;

    AddCustomerRequest addCustomerRequest;

    String amount, transaction_amount_str, bene_name_str, bene_acc_no_str, bank_name_str;

    boolean verificationflags;

    BeneModel bModel;
    TransactionRequest task;
    Wallet2TransactionRequest wallet_task;
    Wallet3TransactionRequest wallet3_task;
    ArrayList<TransactionDetails> transactionDetailsList;
    AddNPayRequest addNPayRequest;
    int amountNumber = 0;
    ProgressDialog pd;
    Handler timer_handler;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_transfer);

        initView();

        rg_add_bene.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_add_imps:
                        transactionMode = "IMPS";
                        break;
                    case R.id.rb_add_neft:
                        transactionMode = "NEFT";
                        break;
                }
            }
        });

        rg_select_bene.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_select_neft:
                        transactionMode = "NEFT";
                        break;
                    case R.id.rb_select_imps:
                        transactionMode = "IMPS";
                        break;
                }
            }
        });

        view_bene_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bene_name.setText("");
                acc_no.setText("");
                bank_name_autocomplete.setText("");
                ifccode.setText("");

                create_add_bene.setVisibility(View.VISIBLE);
                verify_bene.setVisibility(View.VISIBLE);

                single_bene.setVisibility(View.GONE);
                bene_ll.setVisibility(View.VISIBLE);
            }
        });

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserSession();
        String _userName = user.get(session.userName);
        String _userType = user.get(session.userType);
        String _userBalance = user.get(session.userBalance);
        String _adminName = user.get(session.adminName);
        String _mposNumber = user.get(session.mposNumber);

        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);

        //For banklist store in session
//        bank_listSet=session.getBankListinSession();
//        if (bank_listSet==null){
//            retriveBankList();
//        }
//        else {
//            retriveBankList();
//        }
        retriveBankList(); //Call bank list from firestore

        bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadmore.setVisibility(View.VISIBLE);
                load_more.setVisibility(View.VISIBLE);
            }
        });

        create_add_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String _bank_name = bank_name_autocomplete.getText().toString();
                String _ifccode = ifccode.getText().toString();
                String _acc_no = acc_no.getText().toString().replaceAll("-", "");
                String _bene_name = bene_name.getText().toString();

                if (_bank_name.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (_acc_no.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank account number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (_ifccode.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter IFSC code", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (_bene_name.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter beneficiary name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    progress_bene.setVisibility(View.VISIBLE);
                    addBene(mobile_number.getText().toString(), mobile_number.getText().toString(), _bank_name, _acc_no, _bene_name, _ifccode);
                }
            }
        });

        verify_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!customerId.equalsIgnoreCase("") || (customerId != null)) {

                    String _bank_name = bank_name_autocomplete.getText().toString();
                    String _ifccode = ifccode.getText().toString();
                    String _acc_no = acc_no.getText().toString().replaceAll("-", "");
                    String _bene_name = bene_name.getText().toString();


                    if (_bank_name.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank name", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (_acc_no.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank account number", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (_ifccode.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter IFSC code", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (_bene_name.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter beneficiary name", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        progress_bene.setVisibility(View.VISIBLE);
                        verifyBene(customerId, _acc_no, _bank_name, _bene_name, _ifccode);
                    }
                }
            }
        });

        add_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bene_ll.setVisibility(GONE);
                add_new_bene.setVisibility(View.VISIBLE);
                add_verify.setVisibility(View.VISIBLE);
            }
        });

        addAndPayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (amount_transfer.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter transfer amount", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (bank_name_autocomplete.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (acc_no.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank account number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (ifccode.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter IFSC code", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (bene_name.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter beneficiary name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    addandpayConformationDialog();
                }
            }
        });

        show_recept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  bene_ll.setVisibility(View.VISIBLE);
                add_new_bene.setVisibility(View.GONE);
                loadmore.setVisibility(View.GONE);
                load_more.setVisibility(View.GONE);
            }
        });

        show_bene_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bank_name_autocomplete.setText("");
                bene_name.setText("");
                acc_no.setText("");
                bank_name_autocomplete.setText("");
                ifccode.setText("");
                create_add_bene.setVisibility(View.VISIBLE);
                verify_bene.setVisibility(View.VISIBLE);

                bene_ll.setVisibility(View.VISIBLE);
                add_new_bene.setVisibility(View.GONE);
            }
        });


        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommonUtil.hideKeyboard(getApplicationContext(), otp);

                String _otp = otp.getText().toString().trim();
                String _mobile_number = mobile_number.getText().toString();

                if (_otp.isEmpty()) {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Enter valid OTP", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } else if (_mobile_number.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid mobile number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    CommonUtil.hideKeyboard(getApplicationContext(), otp);

                    verifyOtp(_otp, _mobile_number);
                }
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _mobile_number = mobile_number.getText().toString();

                if (_mobile_number.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid mobile number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    resendOtp(_mobile_number);
                }

            }
        });

        mobile_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (count != 10) {
                        main_container.setVisibility(View.GONE);
                        new_customer.setVisibility(View.GONE);
                        verify_number.setVisibility(View.GONE);
                        add_new_bene.setVisibility(View.GONE);
                        single_bene_bank.setVisibility(View.GONE);
                    }
                } catch (Exception exc) {
                    //exc.printStackTrace();
                    main_container.setVisibility(View.GONE);
                    new_customer.setVisibility(View.GONE);
                    verify_number.setVisibility(View.GONE);
                    add_new_bene.setVisibility(View.GONE);
                    single_bene_bank.setVisibility(View.GONE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                try {
                    String mobile = s.toString();

                    if (mobile.length() == 10) {

                        CommonUtil.hideKeyboard(getApplicationContext(), mobile_number);
                        progressBar.setVisibility(View.VISIBLE);

//                        retriveBankList();

                        mActionsListener = new FundTransferPresenter(TransferFragment.this);
                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        });

        amount_to_transfer.setText("Transfer from Wallet 3"); //For default
//        amount_to_transfer.setText("Transfer from Wallet 1");

        enroll_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mob = mobile_number.getText().toString();
                String customerName = customer_name.getText().toString();

                if (mob.isEmpty() && customerName.isEmpty()) {
                    //Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "All fields are required", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } else {
                    addCustomer();
                }
            }
        });

        submit_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = new SessionManager(getApplicationContext());

                _amount_transfer = amount_transfer.getText().toString();

                if (_amount_transfer.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter transfer amount", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    transactionConformationDialog();

                }

            }
        });

        amount_transfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    if (s.toString().isEmpty()) {
                        amouttxt.setText("");
                    } else {
                        String str = Currency.convertToIndianCurrency(s.toString());
                        amouttxt.setText(str);
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }
        });

        bank_name_autocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DMTBankNameActivity.class);
                intent.putExtra("BANKDATA", listOfBank);
                startActivity(intent);
            }
        });

        acc_no.addTextChangedListener(new TextWatcher() {
            int length_before = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                length_before = charSequence.length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                getAccountNo = acc_no.getText().toString();

                if (charSequence.toString().contains("-")) {
                    getAccountNo = charSequence.toString().replace("-", "");
                    System.out.println(">>---->>>>" + getAccountNo);
                }

                //====================================================================================================================================================================//
                // if(activity.getCurrentFocus()==acc_no) {

                if (charSequence.length() == 0) {
                    ifccode.setText("");
                    bene_name.setText("");
                }
                if (selectedBank.length() != 0) {
                    if (!isValidAccount(getAccountNo)) {
                        acc_no.setError("Invalid AccountNo");
                        ifccode.setText("");
                    } else {
                        ifcsFind();
                    }
                } else {
                    bank_name_autocomplete.setError("Please select bank name");
                }

                // }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                // if(activity.getCurrentFocus()==acc_no) {

                if (editable.toString().length() == 0) {
                    ifccode.setText("");
                    bene_name.setText("");
                }
                // }

                if (length_before < editable.length()) {
                    if (editable.length() == 4 || editable.length() == 9 || editable.length() == 14 || editable.length() == 19)
                        editable.append("-");
                    if (editable.length() > 4) {
                        if (Character.isDigit(editable.charAt(4)))
                            editable.insert(4, "-");
                    }
                    if (editable.length() > 9) {
                        if (Character.isDigit(editable.charAt(9)))
                            editable.insert(9, "-");
                    }
                    if (editable.length() > 14) {
                        if (Character.isDigit(editable.charAt(14)))
                            editable.insert(14, "-");
                    }
                    if (editable.length() > 19) {
                        if (Character.isDigit(editable.charAt(19)))
                            editable.insert(19, "-");
                    }
                }

                if (editable.toString().contains("-")) {
                    getAccountNo = editable.toString().replace("-", "");
                    System.out.println(">>---->>>>" + getAccountNo);
                }
            }

        });
    }

    private void addandpayConformationDialog() {
        final Dialog dialog = new Dialog(TransferFragment.this);
        dialog.setContentView(R.layout.transaction_conformation_dialog);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);

        beneMobileTv.setText(mobile_number.getText().toString());
        transactionAmountTv.setText(amount_transfer.getText().toString());
        transactionTypeTv.setText(transactionMode);

        transaction_amount_str = amount_transfer.getText().toString();

        beneNameTv.setText(bene_name.getText().toString());
        beneAccountTv.setText(acc_no.getText().toString());
        bankNameTv.setText(bank_name_autocomplete.getText().toString());

        bene_name_str = bene_name.getText().toString();
        bene_acc_no_str = acc_no.getText().toString();
        bank_name_str = bank_name_autocomplete.getText().toString();

        Button dialogButtonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);

        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doAddNPay();
                dialog.dismiss();
            }
        });

        Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialog_button_no);

        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    private void initView() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("DMT");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        pd = new ProgressDialog(TransferFragment.this);

        single_bene = findViewById(R.id.single_bene);
        bankname = findViewById(R.id.bankname);
        imageView = findViewById(R.id.transfer_nav);
        addbene = findViewById(R.id.addbene);
        bank = findViewById(R.id.bank);
        loadmore = findViewById(R.id.loadmore);
        load_more = findViewById(R.id.submit);
        mobile_number = findViewById(R.id.mobile_number);
        amount_transfer = findViewById(R.id.amount_transfer);
        ifc_code = findViewById(R.id.ifc_code);
        benelistData = findViewById(R.id.benelistData);
        view_bene_list = findViewById(R.id.view_bene_list);
        show_bene_list = findViewById(R.id.show_bene_list);
        pay_add = findViewById(R.id.pay_add);
        benelistData.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        wallet_info = findViewById(R.id.wallet_info);

        wallet_content_one = findViewById(R.id.wallet_content_one);
        wallet_content_two = findViewById(R.id.wallet_content_two);
        wallet_content_three = findViewById(R.id.wallet_content_three);

        wallet_balance_one = findViewById(R.id.wallet_balance_one);
        wallet_balance_two = findViewById(R.id.wallet_balance_two);
        wallet_balance_three = findViewById(R.id.wallet_balance_three);

        add_verify = findViewById(R.id.add_verify);
        wallet_one = findViewById(R.id.wallet_one);
        wallet_two = findViewById(R.id.wallet_two);
        wallet_three = findViewById(R.id.wallet_three);
        main_container = findViewById(R.id.main_container);
        progressBar = findViewById(R.id.progressBar);
        progressBar_main = findViewById(R.id.progressBar_main);
        new_customer = findViewById(R.id.new_customer);
        customer_name = findViewById(R.id.customer_name);
        enroll_number = findViewById(R.id.enroll_number);
        otp = findViewById(R.id.otp);
        verify_number = findViewById(R.id.verify_number);
        resend_otp = findViewById(R.id.resend_otp);
        verify = findViewById(R.id.verify);
        add_bene = findViewById(R.id.add_bene);
        add_new_bene = findViewById(R.id.add_new_bene);
        single_bene_bank = findViewById(R.id.single_bene_bank);
        create_add_bene = findViewById(R.id.create_add_bene);
        bank_name_autocomplete = findViewById(R.id.bank_name_autocomplete);
        acc_no = findViewById(R.id.acc_no);
        ifccode = findViewById(R.id.ifccode);
        bene_name = findViewById(R.id.bene_name);
        amount_to_transfer = findViewById(R.id.amount_to_transfer);
        bene_ll = findViewById(R.id.bene_ll);
        show_recept = findViewById(R.id.show_recept);
        selectedBeneName = findViewById(R.id.selectedBeneName);
        verify_bene = findViewById(R.id.verify_bene);
        progress_bene = findViewById(R.id.progress_bene);
        addAndPayBtn = findViewById(R.id.addAndPayBtn);
        rg_select_bene = findViewById(R.id.rg_select_bene);
        rb_select_neft = findViewById(R.id.rb_select_neft);
        rb_select_imps = findViewById(R.id.rb_select_imps);
        rg_add_bene = findViewById(R.id.rg_add_bene);
        rb_add_imps = findViewById(R.id.rb_add_imps);
        rb_add_neft = findViewById(R.id.rb_add_neft);
        amouttxt = findViewById(R.id.amouttxt);
        submit_pay = findViewById(R.id.pay);

        select_bankName = findViewById(R.id.select_bankName);
        select_ifccode = findViewById(R.id.select_ifccode);

    }

    @Override
    public void onResume() {
        super.onResume();
        // startTimer();
        selectedBank = Constants.BANK_FLAG;
        System.out.println(">>>>>>>:::" + selectedBank);
        bank_name_autocomplete.setText(selectedBank);
        bank_name_autocomplete.setError(null);
        if (selectedBank.length() != 0) {
            if (getAccountNo.length() != 0) {
                if (!isValidAccount(getAccountNo)) {
                    acc_no.setError("Invalid AccountNo");
                    ifccode.setText("");
                } else {
                    ifcsFind();
                }
            }
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 3) {
            String bankName = data.getDataString();
            System.out.println(">>>>>>>:::" + bankName);
        }
    }


    @Override
    public void showWallet(String status, String _customerId, long id, Boolean flag, Double balance, Boolean imps, Boolean neft, Boolean verificationFlag) {

        customerId = _customerId;

        if (mobile_number.getText().toString().length() == 10 && walletInfoStatus) {
            walletInfoStatus = false;
            wallet_info.setVisibility(View.GONE);
        } else {
            walletInfoStatus = true;
            wallet_info.setVisibility(View.VISIBLE);
        }


        if (status.equals("0") && id == 1 && flag) {

            if (imps) {
                paymentMode.add("IMPS");
            }
            if (neft) {
                paymentMode.add("NEFT");
            }

            wallet_content_one.setVisibility(View.VISIBLE);
            wallet_balance_one.setText("₹ " + balance.toString());


            wallet_content_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pipeNo = String.valueOf(id);
                    wallet_selected = 1;
                    verificationflags = verificationFlag;
                    amount_to_transfer.setText("Transfer from Wallet 1 ");
                    pay_add.setVisibility(GONE);
//                    pay_add.setVisibility(View.VISIBLE);
                }
            });
        } else if (status.equals("0") && id == 2) {

            if (imps) {
                paymentMode1.add("IMPS");
            }
            if (neft) {
                paymentMode1.add("NEFT");
            }

            if (!imps && !neft) {
                paymentMode1.add("IMPS");
            }

            wallet_content_two.setVisibility(View.VISIBLE);
            wallet_balance_two.setText("₹ " + balance.toString());

            wallet_content_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pipeNo = String.valueOf(id);
                    verificationflags = verificationFlag;
                    wallet_selected = 2;
                    amount_to_transfer.setText("Transfer from Wallet 2 ");
                    pay_add.setVisibility(GONE);
                }
            });
        } else if (status.equals("0") && id == 3) {

            if (imps) {
                paymentMode2.add("IMPS");
            }

            if (neft) {
                paymentMode2.add("NEFT");
            }

            if (!imps && !neft) {
                paymentMode2.add("IMPS");
                paymentMode2.add("NEFT");
            }

            wallet_content_three.setVisibility(View.VISIBLE);
            wallet_balance_three.setText("₹ " + balance.toString());

            wallet_content_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Snackbar snackbar = Snackbar
//                            .make(coordinatorLayout, "Currently service isn't available", Snackbar.LENGTH_LONG);
//                    snackbar.show();
                    pipeNo = String.valueOf(id);
                    verificationflags = verificationFlag;

                    wallet_selected = 3;
                    amount_to_transfer.setText("Transfer from Wallet 3 ");
//                    pay_add.setVisibility(GONE);
                    pay_add.setVisibility(View.VISIBLE);
                }
            });
        } else {
            wallet_info.setVisibility(View.GONE);
        }
    }


    @Override
    public void showMessage(int message) {


        if (message == 0) {
            progressBar.setVisibility(View.GONE);
            main_container.setVisibility(View.VISIBLE);
            benelistData.setVisibility(View.VISIBLE);
            wallet_info.setVisibility(View.VISIBLE);
            bene_ll.setVisibility(View.VISIBLE);
            single_bene.setVisibility(GONE);


        } else if (message == 12) {
            progressBar.setVisibility(View.GONE);
            new_customer.setVisibility(GONE);
            verify_number.setVisibility(View.VISIBLE);
        } else if (message == 11) {
            new_customer.setVisibility(View.VISIBLE);
            verify_number.setVisibility(GONE);

            progressBar.setVisibility(View.GONE);
            main_container.setVisibility(View.GONE);
            benelistData.setVisibility(View.GONE);
            wallet_info.setVisibility(View.GONE);
        } else {
            verifyOtpDialog();
        }
    }


    @Override
    public void showBeniName(BeneListModel beneListModel) {

        if (beneListModel.getBeneName() == null) {
            bebeAdapter = new BeneRecycleAdapter(TransferFragment.this, beneList);
            benelistData.setAdapter(bebeAdapter);
        } else {
            BeneModel BM = new BeneModel();
            BM.setBeneId(beneListModel.getId());
            BM.setCustomerNumber(beneListModel.getCustomerNumber());
            BM.setBeneMobileNo(beneListModel.getBeneMobileNo());
            BM.setBeneName(beneListModel.getBeneName());
            BM.setAccountNo(beneListModel.getAccountNo());
            BM.setBankName(beneListModel.getBankName());
            BM.setIfscCode(beneListModel.getIfscCode());
            BM.setBeneStatus(beneListModel.getStatus());
            beneList.add(BM);

            bebeAdapter = new BeneRecycleAdapter(TransferFragment.this, beneList);
            benelistData.setAdapter(bebeAdapter);
        }

    }

    @Override
    public void showLoader() {
        try{
            if(pd != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.setMessage("Loading...please wait !");
                        pd.setCancelable(false);
                        pd.show();
                    }
                });
            }
        }catch (Exception e){

        }
    }

    @Override
    public void hideLoader() {
        try {
        if(!isFinishing() && !isDestroyed()) {
            if (pd != null && pd.isShowing()) {

                pd.dismiss();
                pd.cancel();
            }
        }
        }catch (Exception e){

        }
    }

    @Override
    public void status() {
        //progressBar.setVisibility(GONE);
        //verify_number.setVisibility(View.VISIBLE);

        try {
            if (beneList.size() > 0) {
                beneList.clear();
                bebeAdapter.notifyDataSetChanged();
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }


    private void verifyOtpDialog() {
        new_customer.setVisibility(GONE);
        verify_number.setVisibility(View.VISIBLE);
    }

    private void retriveBankList() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                showLoader();
            }
        });
//-----For saving data in session
//        if (bank_listSet!=null){
//
//            listOfBank.addAll(bank_listSet);
//            hideLoader();
//        }
//        else {

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("BankList")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {

                            if (task.isSuccessful()) {

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    List<BankListModel> downloadInfoList = task.getResult().toObjects(BankListModel.class);
                                    bankDataMap.put(document.getId(), downloadInfoList);
                                    listOfBank.add(document.getId());

//                                    Set<String> bankListSet = new HashSet<>();
//                                    bankListSet.addAll(listOfBank);
//                                    session.saveBankListinSession(bankListSet);

                                }

                                for (Map.Entry<String, List<BankListModel>> entry : bankDataMap.entrySet()) {
                                    bankDetailList = new ArrayList<BankListModel>(entry.getValue());
                                }

                                String _bankDetailList = new Gson().toJson(bankDetailList);
                                String _listOfBank = new Gson().toJson(listOfBank);

//                            Log.i("_bankDetailList :: ", _bankDetailList);
//                            Log.i("_listOfBank :: ", _listOfBank);

                                hideLoader();

//                            progressBar.setVisibility(View.GONE);
//                            main_container.setVisibility(View.VISIBLE);
//                            benelistData.setVisibility(View.VISIBLE);

                            } else {
                                hideLoader();

                                //       Log.w("sdd", "Error getting documents.", task.getException());
                            }
                        }
                    });

//        }
    }

    private void addCustomer() {

        String mob = mobile_number.getText().toString();
        String customerName = customer_name.getText().toString();

        if (customerName != null && !customerName.isEmpty()) {

            addCustomerRequest = new AddCustomerRequest(customerName, mob);

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(ADD_CUSTOMER)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }
                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedAccCustomer(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } else {
            //  Log.d("STATUS", "TransactionSuccesscustomername1" + customerName);
            //Toast.makeText(getApplicationContext(), "Please enter customer name", Toast.LENGTH_LONG).show();
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Please enter customer name", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    public void encriptedAccCustomer(String encodedUrl) {
        final AddCustomerAPI addCustomerAPI = this.apiFundTransferService.getClient().create(AddCustomerAPI.class);

        addCustomerAPI.getAddCustomer(_token, addCustomerRequest, encodedUrl).enqueue(new Callback<AddCustomerResponse>() {
            @Override
            public void onResponse(Call<AddCustomerResponse> call, Response<AddCustomerResponse> response) {
                if (response.isSuccessful()) {
                    //.d("STATUS", "TransactionSuccessfulAddCustomer" + response.body());
                    verifyOtpDialog();
                    //hideLoader();
                } else {
                    // Log.d("STATUS", "TransactionFailedAddCustomer" + response.body());
                    // hideLoader();
                }
            }

            @Override
            public void onFailure(Call<AddCustomerResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void verifyOtp(String otp, final String mob) {
        showLoader();

        final VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest(otp, mob);

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(VERIFY_OTP)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }
                            System.out.println(">>>>-----" + encodedUrl);
                            encriptedVerifyOtp(verifyOtpRequest, encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });

    }

    public void encriptedVerifyOtp(VerifyOtpRequest verifyOtpRequest, String encodedUrl) {
        final VerifyOtpApi verifyOtpApi = this.apiFundTransferService.getClient().create(VerifyOtpApi.class);

        verifyOtpApi.getVerifyOtp(_token, verifyOtpRequest, encodedUrl).enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {

                if (response.isSuccessful()) {
                    progressBar.setVisibility(GONE);
                    hideLoader();

                    if (response.body().getStatus().equals("0")) {
                        verify_number.setVisibility(GONE);
                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "" + response.body().getStatusDesc(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    // Log.d("STATUS", "TransactionSuccessfulVerifyOtp" + response.body());
                } else {
                    progressBar.setVisibility(GONE);
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void resendOtp(String mob) {

        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("number", mob);

            AndroidNetworking.post(RESEND_OTP)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }
                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedResendOtp(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void encriptedResendOtp(String encodedUrl) {

        final ResendOtpAPI resendOtpAPI = this.apiFundTransferService.getClient().create(ResendOtpAPI.class);

        resendOtpAPI.getResendOtpReport(_token, encodedUrl).enqueue(new Callback<ResendOtpResponse>() {
            @Override
            public void onResponse(Call<ResendOtpResponse> call, Response<ResendOtpResponse> response) {
                if (response.isSuccessful()) {
                    hideLoader();
                    //  Log.d("STATUS", "TransactionSuccessfulResendOtp" + response.body());
                } else {
                    hideLoader();
                    // Log.d("STATUS", "TransactionFailedResendOtp" + response.body());
                }
            }

            @Override
            public void onFailure(Call<ResendOtpResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    public void itemOncliclickView(BeneModel bModel) {
        try {
            this.bModel = bModel;

            bene_ll.setVisibility(View.GONE);
            single_bene.setVisibility(View.VISIBLE);
            select_bankName.setText(bModel.beneName);
            select_ifccode.setText(bModel.ifscCode);

            beneId = bModel.beneId;

        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    public void itemOncliclickDelete(BeneModel bModel, int position) {

        showLoader();

        try {
            AndroidNetworking.get(DELETE_BENE_DMT)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                System.out.println(">>>>-----" + encodedUrl);
                                enCriptedDeleteBene(mobile_number.getText().toString(), bModel.getBeneId(), encodedUrl, position);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    private void enCriptedDeleteBene(String custtomerNo, String beneIdStr, String encodedUrl, int position) {

        JSONObject obj = new JSONObject();

        try {
            obj.put("beneId", beneIdStr);
            obj.put("customerNumber", custtomerNo);

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .setContentType("application/json; charset=utf-8")
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                hideLoader();
                                JSONObject obj = new JSONObject(response.toString());
                                //String key = obj.getString("hello");
                                System.out.println(">>>>-----" + response.toString());

                                beneList.remove(position);
                                bebeAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidAccount(String accountPattern) {
        String patternAccountNo = "";
        for (String bankl : listOfBank) {

            if (bankl.equalsIgnoreCase(selectedBank)) {

                int pos = listOfBank.indexOf(selectedBank);

                String strr = bankDetailList.get(pos).getPATTERN();

                if (strr != null) {
                    patternAccountNo = strr;
                }
            }
        }
        Pattern pattern = Pattern.compile(patternAccountNo);
        Matcher matcher = pattern.matcher(accountPattern);
        return matcher.matches();
    }

    private void ifcsFind() {

        for (String bankl : listOfBank) {

            if (bankl.equalsIgnoreCase(selectedBank)) {
                int pos = listOfBank.indexOf(selectedBank);
                String flag = bankDetailList.get(pos).getFLAG();
                String bankCode = bankDetailList.get(pos).getBANKCODE();
                if (flag != null) {
                    if (flag.equalsIgnoreCase("u")) {
                        ifccode.setText(bankCode);
                        ifccode.setEnabled(false);
                    } else if (flag.equalsIgnoreCase("4")) {
                        String first4 = "";
                        try {

                            first4 = getAccountNo.substring(0, 4);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ifccode.setText(bankCode + first4);
                        ifccode.setEnabled(false);
                    } else if (flag.equalsIgnoreCase("3")) {

                        String first3 = "";
                        try {

                            first3 = getAccountNo.substring(0, 3);
                            //first3 = enterAccountNo.getText().toString().substring(0,3);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ifccode.setText(bankCode + first3);
                        ifccode.setEnabled(false);
                    } else if (flag.equalsIgnoreCase("6")) {
                        String first6 = "";
                        try {
                            first6 = getAccountNo.substring(0, 6);
                            // first6 = enterAccountNo.getText().toString().substring(0,6);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ifccode.setText(bankCode + first6);
                        ifccode.setEnabled(false);
                    }
                } else /*if(bankl.getFLAG().equalsIgnoreCase(null))*/ {
                    ifccode.setText("no IFSC");
                    ifccode.setEnabled(true);
                }
            }
        }
    }

    private void doAddNPay() {
        progress_bene.setVisibility(View.VISIBLE);

        addNPayRequest = new AddNPayRequest(amount_transfer.getText().toString().trim(), mobile_number.getText().toString().trim(), transactionMode, bene_name.getText().toString(), ifccode.getText().toString(), bank_name_autocomplete.getText().toString(), acc_no.getText().toString().replaceAll("-", ""), mobile_number.getText().toString(), pipeNo);

        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(ADD_AND_PAY)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }

                            System.out.println(">>>>-----" + encodedUrl);

                            encriptedAddAndPay(encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });

    }

    public void encriptedAddAndPay(String encodedUrl) {
        AddNPayAPI addNPayAPI = this.apiFundTransferService.getClient().create(AddNPayAPI.class);
        addNPayAPI.getAddNPayReport(_token, addNPayRequest, encodedUrl).enqueue(new Callback<AddNPayResponse>() {
            @Override
            public void onResponse(Call<AddNPayResponse> call, Response<AddNPayResponse> response) {
                if (response.isSuccessful()) {

                    hideLoader();

                    progress_bene.setVisibility(GONE);

                    // Log.d("STATUS", "TransactionSuccessfulAddNPAY" + response.body());

                    mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                    transactionDetailsList = new ArrayList<TransactionDetails>();
                    TransactionDetails transactionDetails = new TransactionDetails();
                    transactionDetails.setTransaction_amount(String.valueOf(amount_transfer.getText().toString()));
                    transactionDetails.setTracking_no(response.body().gettxnId());
                    transactionDetails.setTransaction_type(transactionMode);
                    transactionDetailsList.add(transactionDetails);
                    transactionSuccessDialog(response.body().gettxnId(), response.body().getstatusDesc(), transactionDetailsList);

                } else {

                    try {
                        progress_bene.setVisibility(GONE);

                        hideLoader();
                        Gson gson = new Gson();
                        MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                        try {
                            showPaymentFailedDialog(message.getMessage());
                        } catch (Exception exc) {
                            //exc.printStackTrace();
                            showPaymentFailedDialog("Transaction failed");
                        }

//                        Gson gson = new Gson();
//                        MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
//                        showPaymentFailedDialog(message.getMessage());

                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<AddNPayResponse> call, Throwable t) {
                hideLoader();
            }
        });
    }

    private void addBene(String _mobile_number, String mobile_number, String _bank_name, String getAccountNo, String _bene_name, String _ifccode) {

        showLoader();

        final AddBeneRequest addBeneRequest = new AddBeneRequest(_mobile_number, mobile_number, _bank_name, getAccountNo.replaceAll("-", ""), _bene_name, _ifccode);

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(ADD_BENE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }
                            System.out.println(">>>>-----" + encodedUrl);
                            encriptAddBene(addBeneRequest, apiFundTransferService, encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        hideLoader();
                    }
                });

    }

    public void encriptAddBene(AddBeneRequest addBeneRequest, APIFundTransferService apiFundTransferService, String encodedUrl) {

        final AddBeneAPI addBeneAPI = this.apiFundTransferService.getClient().create(AddBeneAPI.class);
        addBeneAPI.getAddBene(_token, addBeneRequest, encodedUrl).enqueue(new Callback<AddBeneResponse>() {
            @Override
            public void onResponse(Call<AddBeneResponse> call, Response<AddBeneResponse> response) {
                if (response.isSuccessful()) {
                    // Log.d("STATUS", "TransactionSuccessfulAddBene" + response.body());

                    progress_bene.setVisibility(GONE);

                    bank_name_autocomplete.setText("");
                    bene_name.setText("");
                    acc_no.setText("");
                    ifccode.setText("");

                    create_add_bene.setVisibility(View.VISIBLE);
                    verify_bene.setVisibility(View.VISIBLE);
                    add_new_bene.setVisibility(View.GONE);
                    bene_ll.setVisibility(View.VISIBLE);

                    hideLoader();

                    mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                    //Toast.makeText(getApplicationContext(), response.body().getStatusDesc(), Toast.LENGTH_LONG).show();

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "" + response.body().getStatusDesc(), Snackbar.LENGTH_LONG);
                    snackbar.show();


                } else {
                    progress_bene.setVisibility(GONE);
                    // Log.d("STATUS", "TransactionFailedAddBene" + response.body());

                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<AddBeneResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void verifyBene(String customerId, String getAccountNo, String bankName, String beneNameEnter, String ifscCode) {

        if (customerId == null || getAccountNo == null || bankName == null || beneNameEnter == null || ifscCode == null) {
            // Toast.makeText(getApplicationContext(), "Beneficiary details field are empty", Toast.LENGTH_LONG).show();

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Beneficiary details field are empty", Snackbar.LENGTH_LONG);
            snackbar.show();

        } else {

            if ((pipeNo.equals("3"))) {
                pipeNo = "3";
            } else if ((pipeNo.equals("1"))) {
                pipeNo = "1";
            } else {
                pipeNo = "2";
            }

            final VerifyBeneRequest verifyBeneRequest = new VerifyBeneRequest(customerId, getAccountNo, bankName, beneNameEnter, ifscCode, "1");


            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(VERIFY_BENE_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }
                                System.out.println(">>>>-----" + encodedUrl);
                                enCriptedVerifyBene(verifyBeneRequest, encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }
    }

    public void enCriptedVerifyBene(VerifyBeneRequest verifyBeneRequest, String encodedUrl) {

        final VerifyBeneApi verifyBeneApi = this.apiFundTransferService.getClient().create(VerifyBeneApi.class);

        verifyBeneApi.getVerifyBene(_token, verifyBeneRequest, encodedUrl).enqueue(new Callback<VerifyBeneResponse>() {
            @Override
            public void onResponse(Call<VerifyBeneResponse> call, Response<VerifyBeneResponse> response) {

                if (response.isSuccessful()) {
                    //  Log.d("STATUS", "Transaction successful VerifyBene" + response.body());

                    progress_bene.setVisibility(GONE);

                    //add_verify.setVisibility(GONE);

                    bene_name.setText(response.body().getBeneName());

                    //Toast.makeText(getApplicationContext(), response.body().getStatusDesc(), Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "" + response.body().getStatusDesc(), Snackbar.LENGTH_LONG);

                    snackbar.show();


                    //                    bene_name.setText(response.body().getBeneName());
                    //                    bank_name_autocomplete.setText("");
                    //                    bene_name.setText("");
                    //                    acc_no.setText("");
                    //                    bank_name_autocomplete.setText("");
                    //                    ifccode.setText("");
                    //                    create_add_bene.setVisibility(View.VISIBLE);
                    //                    verify_bene.setVisibility(View.VISIBLE);
                    //
                    //                    bene_ll.setVisibility(View.VISIBLE);
                    //                    add_new_bene.setVisibility(View.GONE);
                    //hideLoader();

                } else {
                    //hideLoader();
                    progress_bene.setVisibility(GONE);

                    //  Log.d("STATUS", "Transaction Failed Verify Bene" + response.body());
                    //hideLoader();
                }
            }

            @Override
            public void onFailure(Call<VerifyBeneResponse> call, Throwable t) {
                //hideLoader();
                progress_bene.setVisibility(GONE);
            }
        });

    }

    private void transaction(String pipeNO) { //for wallet 1

        if (pipeNO.equalsIgnoreCase("1")){

            showLoader();


            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(TRANSACTION)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedTransaction(encodedUrl,pipeNO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }
        else {

//            Toast.makeText(this, "For wallet 3 transaction", Toast.LENGTH_SHORT).show();


            showLoader();


            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(WALLET3_TRANSACTION_API)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedTransaction(encodedUrl,pipeNO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }


    }

    public void encriptedTransaction(String encodedUrl,String pipeNo) {

        if (pipeNo.equalsIgnoreCase("1")){


            final TransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(TransactionAPI.class);
            transactionAPI.getTransectionReport(_token, task, encodedUrl).enqueue(new Callback<TransactionResponse>() {
                @Override
                public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                    if (response.isSuccessful()) {

                        hideLoader();

                        int amountNumber = 0;
                        String amount = amount_transfer.getText().toString();
                        try {
                            amountNumber = Integer.parseInt(amount.trim());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                        transactionDetailsList = new ArrayList<TransactionDetails>();
                        TransactionDetails transactionDetails = new TransactionDetails();
                        transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                        transactionDetails.setTracking_no(response.body().getTxnId());
                        transactionDetails.setTransaction_type(transactionMode);
                        transactionDetailsList.add(transactionDetails);

                        transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);


                    } else {

                        try {
                            hideLoader();

                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                            //transaction failed

                            try {
                                showPaymentFailedDialog(message.getMessage());
                            } catch (Exception exc) {
                                //exc.printStackTrace();

                            }

                        }catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<TransactionResponse> call, Throwable t) {
                    hideLoader();
                }
            });


        }
        else {
        //For wallet3

            final Wallet3TransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(Wallet3TransactionAPI.class);
            transactionAPI.getTransectionReport(_token, wallet3_task, encodedUrl).enqueue(new Callback<TransactionResponse>() {
                @Override
                public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                    if (response.isSuccessful()) {

                        hideLoader();

                        int amountNumber = 0;
                        String amount = amount_transfer.getText().toString();
                        try {
                            amountNumber = Integer.parseInt(amount.trim());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                        transactionDetailsList = new ArrayList<TransactionDetails>();
                        TransactionDetails transactionDetails = new TransactionDetails();
                        transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                        transactionDetails.setTracking_no(response.body().getTxnId());
                        transactionDetails.setTransaction_type(transactionMode);
                        transactionDetailsList.add(transactionDetails);

                        transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);


                    } else {

                        try {
                            hideLoader();

                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                            //transaction failed

                            try {
                                showPaymentFailedDialog(message.getMessage());
                            } catch (Exception exc) {
                                //exc.printStackTrace();

                            }

                        }catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<TransactionResponse> call, Throwable t) {
                    hideLoader();
                }
            });



        }




    }

    private void wallet2transaction() { //for wallet2

        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(WALLET2_TRANSACTION_API)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }
                            System.out.println(">>>>-----" + encodedUrl);
                            wallet2encriptedTransaction(encodedUrl);


                        } catch (JSONException e) {
                            //e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            //e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });
    }

    public void wallet2encriptedTransaction(String encodedUrl) {

        final Wallet2TransactionAPI wallet2TransactionAPI = this.apiFundTransferService.getClient()
                .create(Wallet2TransactionAPI.class);
        wallet2TransactionAPI.getTransectionReport(_token, wallet_task, encodedUrl)
                .enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                if (response.isSuccessful()) {

                    hideLoader();

                    transactionDetailsList = new ArrayList<TransactionDetails>();
                    TransactionDetails transactionDetails = new TransactionDetails();
                    transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                    transactionDetails.setTracking_no(response.body().getTxnId());
                    transactionDetails.setTransaction_type(transactionMode);
                    transactionDetailsList.add(transactionDetails);

                    transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);

                } else {

                    try {

                        hideLoader();
                        Gson gson = new Gson();
                        MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                        try {
                            showPaymentFailedDialog(message.getMessage());
                        } catch (Exception exc) {
                            //exc.printStackTrace();
                            showPaymentFailedDialog("Transaction failed");
                        }

                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                hideLoader();
            }
        });
    }

    private void transactionConformationDialog() {
        final Dialog dialog = new Dialog(TransferFragment.this);
        dialog.setContentView(R.layout.transaction_conformation_dialog);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);

        beneMobileTv.setText(mobile_number.getText().toString());
        transactionAmountTv.setText(amount_transfer.getText().toString());
        transactionTypeTv.setText(transactionMode);

        transaction_amount_str = amount_transfer.getText().toString();

        beneNameTv.setText(bModel.getBeneName());
        beneAccountTv.setText(bModel.getAccountNo());
        bankNameTv.setText(bModel.getBankName());
        bene_name_str = bModel.getBeneName();
        bene_acc_no_str = bModel.getAccountNo();
        bank_name_str = bModel.getBankName();

        Button dialogButtonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);

        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conformTransaction();
                dialog.dismiss();
            }
        });

        Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialog_button_no);

        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //hideLoader();

    }

    private void conformTransaction() {

//        Log.i("pipeNo ::: ", "" + pipeNo);
//        Log.i("amount_str ::", transaction_amount_str);
//        Log.i("customerId", customerId);
//        Log.i("beneId", beneId);
//        Log.i("transactionMode", transactionMode);


        if (pipeNo.equalsIgnoreCase("2")) { //For wallet 2

            transaction_amount_str = amount_transfer.getText().toString();
            wallet_task = new Wallet2TransactionRequest(transaction_amount_str, customerId, pipeNo, beneId, transactionMode, "");
            wallet2transaction();
        }
        else if (pipeNo.equalsIgnoreCase("3")){  //For wallet 3

            transaction_amount_str = amount_transfer.getText().toString();
            wallet3_task = new Wallet3TransactionRequest(amount_transfer.getText().toString(), customerId, pipeNo, beneId, transactionMode);

            try {
                amountNumber = Integer.parseInt(transaction_amount_str.trim());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
            }

            if (amountNumber <= 5000) {
                transaction("3");
            } else {
                bulkTransfer("3");
            }

        }
        else{   //For pipeNo-1

            transaction_amount_str = amount_transfer.getText().toString();
            task = new TransactionRequest(amount_transfer.getText().toString(), customerId, pipeNo, beneId, transactionMode);

            try {
                amountNumber = Integer.parseInt(transaction_amount_str.trim());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
            }

            if (amountNumber <= 5000) {
                transaction("1");
            } else {
                bulkTransfer("1");
            }

        }

    }

    private void transactionSuccessDialog(final String txnId, String statusDesc, final ArrayList<TransactionDetails> transactionDetailsList) {
        //wallet_info.setVisibility(GONE);

        final Dialog dialog = new Dialog(TransferFragment.this);
        dialog.setContentView(R.layout.transaction_success_dialog);
        dialog.setCancelable(false);

        final TextView tv = dialog.findViewById(R.id.tv);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);
        final TextView trackingId = dialog.findViewById(R.id.tracking_no_tv);

        if (txnId == null || txnId.trim().length() == 0) {
            tv.setBackgroundColor(getResources().getColor(R.color.red));
        } else {
            tv.setBackgroundColor(getResources().getColor(R.color.green));
        }

        tv.setText(statusDesc);
        trackingId.setText(txnId);
        beneMobileTv.setText(mobile_number.getText().toString());
        transactionAmountTv.setText(transaction_amount_str);
        transactionTypeTv.setText(transactionMode);

        beneNameTv.setText(bene_name_str);
        beneAccountTv.setText(bene_acc_no_str);
        bankNameTv.setText(bank_name_str);

        Button dialogButtonOK = (Button) dialog.findViewById(R.id.dialog_button_no);
        Button dialogPrint = (Button) dialog.findViewById(R.id.dialog_button_print);

        dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mActionsListener.loadWallet(_token, mobile_number.getText().toString());
                clearViewList();
            }
        });

        dialog.show();

        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        Button dialogbuttonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);
        dialogbuttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (com.isuisudmt.utils.Constants.BRAND_NAME.trim().length() != 0) {
                    dialog.dismiss();
                    Intent intent = new Intent(TransferFragment.this, CreatePDFActivity.class);
                    intent.putExtra("data", transactionDetailsList);
                    intent.putExtra("acc_no", bene_acc_no_str);
                    intent.putExtra("bene_name", bene_name_str);
                    intent.putExtra("bank_name", bank_name_str);
                    intent.putExtra("bene_mob_no", mobile_number.getText().toString());
                    startActivity(intent);
                } else {
                    showBrandSetAlert();
                }
            }
        });

        dialogPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (com.isuisudmt.utils.Constants.BRAND_NAME.trim().length() != 0) {
                    BluetoothDevice bluetoothDevice = Constants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        callBluetoothFunction(bene_name_str, mobile_number.getText().toString(), bene_acc_no_str, bank_name_str, transaction_amount_str, txnId, transactionMode, bluetoothDevice);
                    } else {
                        Intent in = new Intent(TransferFragment.this, MainActivity.class);
                        startActivity(in);
                        Toast.makeText(TransferFragment.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showBrandSetAlert();
                }

            }
        });
    }
    private void showBrandSetAlert(){
        try{
            AlertDialog.Builder builder1 = new AlertDialog.Builder(TransferFragment.this);
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }catch (Exception e){

        }
    }
    private void callBluetoothFunction(final String bene_name,final String bene_mob,final String bene_acc,final String bank_name,final String amount,final String tid,final String type,BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(Constants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                mPrinter.printText("--------Transaction Report---------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.printText("Bene Name: "+bene_name);
                mPrinter.addNewLine();
                mPrinter.printText("Bene Mobile No.: "+bene_mob);
                mPrinter.addNewLine();
                mPrinter.printText("Bene Account No.: "+bene_acc);
                mPrinter.addNewLine();
                mPrinter.printText("Bank Name.: "+bank_name);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Amount: "+amount);
                mPrinter.addNewLine();
                mPrinter.printText("API Tid: "+tid);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Type: "+type);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(Constants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
                Toast.makeText(TransferFragment.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showPaymentFailedDialog(String message) {

        try {
            final Dialog dialog = new Dialog(TransferFragment.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.transaction_successful_custom_dialog);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setAttributes(lp);

            Window window = dialog.getWindow();

            final TextView transaction_status = (TextView) dialog.findViewById(R.id.transaction_status);
            final TextView trandsaction_id = (TextView) dialog.findViewById(R.id.trandsaction_id);
            final Button dialogButtonConfirm = (Button) dialog.findViewById(R.id.dialogButtonConfirm);

            transaction_status.setText("Transaction failed");
            transaction_status.setBackgroundColor(getResources().getColor(R.color.color_report_red));
            trandsaction_id.setText(message);

            dialogButtonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }catch (Exception e){

        }
    }

    private void bulkTransfer(String pipeNo) {

        if (pipeNo.equalsIgnoreCase("1")) {

            showLoader();

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(BULK_TRANSFER)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);

                                encriptedBulkTransfer(encodedUrl,pipeNo);

                            } catch (JSONException e) {
                                //e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                // e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }

        else {
            //For wallet 3

            showLoader();

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(BULK_TRANSFER_WALLET3)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);

                                encriptedBulkTransfer(encodedUrl,pipeNo);

                            } catch (JSONException e) {
                                //e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                // e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });


        }

    }

    public void encriptedBulkTransfer(String encodedUrl,String pipeNo) {

        if (pipeNo.equalsIgnoreCase("1")) {

            BulkTransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(BulkTransactionAPI.class);

            transactionAPI.getTransectionReport(_token, task, encodedUrl).enqueue(new Callback<BulkTransactionResponse>() {

                @Override
                public void onResponse(Call<BulkTransactionResponse> call, Response<BulkTransactionResponse> response) {
                    try {
                        if (response.body().getTransactionResponse() != null) {

                            if (response.isSuccessful()) {

                                hideLoader();

//                          Log.i("body :: ", "" + response.body().getTransactionResponse());

                                ArrayList<TransactionResponse> BulkList = response.body().getTransactionResponse();

                                String txnId = "";

                                transactionDetailsList = new ArrayList<TransactionDetails>();

                                int amountTemp = amountNumber;

                                for (int i = 0; i < BulkList.size(); i++) {
                                    TransactionDetails transactionDetails = new TransactionDetails();
                                    transactionDetails.setTracking_no(BulkList.get(i).getTxnId());
                                    transactionDetails.setTransaction_type(transactionMode);

                                    if (amountTemp > 5000) {
                                        amountTemp = amountTemp - 5000;
                                        transactionDetails.setTransaction_amount("5000");
                                    } else {
                                        transactionDetails.setTransaction_amount(String.valueOf(amountTemp));
                                    }

                                    transactionDetailsList.add(transactionDetails);

                                    if (i == 0) {
                                        txnId = BulkList.get(i).getTxnId();
                                    } else {
                                        txnId = txnId + "," + BulkList.get(i).getTxnId();
                                    }
                                }

                                transactionSuccessDialog(txnId, BulkList.get(1).getStatusDesc(), transactionDetailsList);
                            } else {

                                try {
                                    hideLoader();
                                    Gson gson = new Gson();
                                    MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);

                                    try {
                                        showPaymentFailedDialog(message.getMessage());
                                    } catch (Exception e) {

                                        showPaymentFailedDialog("Transaction failed");
                                    }
                                } catch (Exception e) {

                                }
                            }
                        } else {
                            hideLoader();
                            showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");
                        }
                    } catch (Exception exc) {
                        //exc.printStackTrace();
                        hideLoader();
                        showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");

                    }
                }

                @Override
                public void onFailure(Call<BulkTransactionResponse> call, Throwable t) {
                    hideLoader();
                    showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");
                }
            });

        }
        else {

            //For wallet3

            BulkTransfer_Wallet3_TransactionAPi transactionAPI = this.apiFundTransferService.getClient().create(BulkTransfer_Wallet3_TransactionAPi.class);

            transactionAPI.getTransectionReport(_token, wallet3_task, encodedUrl).enqueue(new Callback<BulkTransactionResponse>() {

                @Override
                public void onResponse(Call<BulkTransactionResponse> call, Response<BulkTransactionResponse> response) {
                    try {
                        if (response.body().getTransactionResponse() != null) {

                            if (response.isSuccessful()) {

                                hideLoader();

//                          Log.i("body :: ", "" + response.body().getTransactionResponse());

                                ArrayList<TransactionResponse> BulkList = response.body().getTransactionResponse();

                                String txnId = "";

                                transactionDetailsList = new ArrayList<TransactionDetails>();

                                int amountTemp = amountNumber;

                                for (int i = 0; i < BulkList.size(); i++) {
                                    TransactionDetails transactionDetails = new TransactionDetails();
                                    transactionDetails.setTracking_no(BulkList.get(i).getTxnId());
                                    transactionDetails.setTransaction_type(transactionMode);

                                    if (amountTemp > 5000) {
                                        amountTemp = amountTemp - 5000;
                                        transactionDetails.setTransaction_amount("5000");
                                    } else {
                                        transactionDetails.setTransaction_amount(String.valueOf(amountTemp));
                                    }

                                    transactionDetailsList.add(transactionDetails);

                                    if (i == 0) {
                                        txnId = BulkList.get(i).getTxnId();
                                    } else {
                                        txnId = txnId + "," + BulkList.get(i).getTxnId();
                                    }
                                }

                                transactionSuccessDialog(txnId, BulkList.get(1).getStatusDesc(), transactionDetailsList);
                            } else {

                                try {
                                    hideLoader();
                                    Gson gson = new Gson();
                                    MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);

                                    try {
                                        showPaymentFailedDialog(message.getMessage());
                                    } catch (Exception e) {

                                        showPaymentFailedDialog("Transaction failed");
                                    }
                                } catch (Exception e) {

                                }
                            }
                        } else {
                            hideLoader();
                            showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");
                        }
                    } catch (Exception exc) {
                        //exc.printStackTrace();
                        hideLoader();
                        showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");

                    }
                }

                @Override
                public void onFailure(Call<BulkTransactionResponse> call, Throwable t) {
                    hideLoader();
                    showPaymentFailedDialog(" * You can see your transaction details on fund transfer report section.");
                }
            });


        }

    }

    public void clearViewList() {
        //mobile_number.setText("");
        amount_transfer.setText("");
        customer_name.setText("");
        otp.setText("");

        ifc_code.setText("");
        acc_no.setText("");
        ifccode.setText("");
        bene_name.setText("");
        bank_name_autocomplete.setText("");
        ifccode.setText("");
    }

 /*   @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        timer_handler.removeCallbacksAndMessages(null);
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer_handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer_handler.removeCallbacksAndMessages(null);
    }

    public void startTimer() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        timer_handler.removeCallbacksAndMessages(null);

                        if (!isFinishing()) {
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }

    public void showSessionAlert() {

        pd.dismiss();

        AlertDialog.Builder alertbuilderupdate;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }

        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(TransferFragment.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
         }).show();

    }*/

}