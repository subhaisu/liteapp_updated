package com.isuisudmt.dmt;

public class AddBeneResponse {

    private String status;

    private BeneListModel[] beneList;

    private String statusDesc;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public BeneListModel[] getBeneList ()
    {
        return beneList;
    }

    public void setBeneList (BeneListModel[] beneList)
    {
        this.beneList = beneList;
    }

    public String getStatusDesc ()
    {
        return statusDesc;
    }

    public void setStatusDesc (String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", beneList = "+beneList+", statusDesc = "+statusDesc+"]";
    }

}
