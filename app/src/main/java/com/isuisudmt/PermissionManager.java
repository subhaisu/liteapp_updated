package com.isuisudmt;

import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;


public class PermissionManager {

    private final AppCompatActivity activity;
    private int requestCode;
    private String permission;
    private OnRequestPermissionListener permissionListener;

    /**
     * Gets instance.
     *
     * @param activity the activity
     * @return the instance
     */
    public static PermissionManager getInstance(AppCompatActivity activity) {
        synchronized (PermissionManager.class) {
            return new PermissionManager(activity);
        }
    }

    /**
     * Instantiates a new Permission manager.
     *
     * @param activity the activity
     */
    public PermissionManager(AppCompatActivity activity) {
        this.activity = activity;
    }

    /**
     * Load permission.
     *
     * @param permission  the permission
     * @param requestCode the request code
     */
    public void loadPermission(String permission, int requestCode) {
        this.permission = permission;
        this.requestCode = requestCode;
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
        } else {
            if (permissionListener != null)
                permissionListener.onRequestGranted(requestCode, permission);
        }
    }

    //---------------------

    public void checkAndRequestPermissions(AppCompatActivity activity, String... permissions) {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {

                listPermissionsNeeded.add(permission);
            }else{
                if(!listPermissionsNeeded.isEmpty()){
                    permissionListener.onRequestGranted(requestCode, permission);
                }

            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
        }
    }





    /**
     * On request permissions result.
     *
     * @param requestCode  the request code
     * @param permissions  the permissions
     * @param grantResults the grant results
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == this.requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissionListener != null)
                    permissionListener.onRequestGranted(requestCode, permission);
            } else {
                if (permissionListener != null)
                    permissionListener.onRequestDeclined(requestCode, permission);
            }
        }
    }

    /**
     * Sets on request permission listener.
     *
     * @param permissionListener the permission listener
     */
    public void setOnRequestPermissionListener(OnRequestPermissionListener permissionListener) {
        this.permissionListener = permissionListener;
    }

    /**
     * The interface On request permission listener.
     */
    public interface OnRequestPermissionListener {
        /**
         * On request granted.
         *
         * @param requestCode the request code
         * @param permission  the permission
         */
        void onRequestGranted(int requestCode, String permission);

        /**
         * On request declined.
         *
         * @param requestCode the request code
         * @param permission  the permission
         */
        void onRequestDeclined(int requestCode, String permission);
    }


}
