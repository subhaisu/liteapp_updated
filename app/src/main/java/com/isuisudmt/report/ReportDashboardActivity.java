package com.isuisudmt.report;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.matm.MATMActivity;

import java.util.HashMap;

import static com.isuisudmt.Constants.session_time;


public class ReportDashboardActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private LinearLayout fragment_container;
    private TabLayout tablayout;


    Handler timer_handler;
    String _userName;
    boolean session_logout = false;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_dashboard);
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);

        setUpToolBar();
        initView();
        callTablistener();
    }


    private void setUpToolBar() {
        // Inflate the layout for this fragment
        mToolbar = findViewById ( R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle("");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //What to do on back clicked
            }
        });
    }
    private void initView() {
        tablayout = findViewById(R.id.tablayout);
        fragment_container = findViewById(R.id.fragment_container);


    }

    private void callTablistener() {
        replaceFragment(new AepsFragment());
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    replaceFragment(new AepsFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("AePS Report");
                } else if (tab.getPosition() == 1) {
                    replaceFragment(new MatmFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("mATM Report");
                }
                else if(tab.getPosition() == 2) {
                    replaceFragment(new DmtFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("DMT Report");
                }
                else if(tab.getPosition() == 3){
                    replaceFragment(new CashOutFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Cashout");
                }
                else  {
                    replaceFragment(new WalletFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Wallet");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.commit();
    }


/*    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if(_userName!=null && session_logout == false) {
            checkSessionExistance(_userName,true);
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
        //startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(ReportDashboardActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(ReportDashboardActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(ReportDashboardActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(ReportDashboardActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/


}
