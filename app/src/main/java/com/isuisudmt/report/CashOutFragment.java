package com.isuisudmt.report;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.cashoutReport.CashoutReportContract;
import com.isuisudmt.cashoutReport.CashoutReportModel;
import com.isuisudmt.cashoutReport.CashoutReportPresenter;
import com.isuisudmt.cashoutReport.CashoutReportRecyclerViewAdapter;
import com.isuisudmt.matm.MicroReportContract;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.MicroReportPresenter;
import com.isuisudmt.matm.MicroReportRecyclerViewAdapter;
import com.isuisudmt.matm.RefreshModel;
import com.isuisudmt.matm.TransactionStatusModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class CashOutFragment extends Fragment implements CashoutReportContract.View, DatePickerDialog.OnDateSetListener {

    private RecyclerView reportRecyclerView;
    private CashoutReportPresenter mActionsListener;
    ArrayList<CashoutReportModel> reportResponseArrayList ;
    LinearLayout dateLayout,detailsLayout;
    private CashoutReportRecyclerViewAdapter cashoutReportRecyclerViewAdapter;
    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    String fromdate = "";
    String todate = "";
    String clientId = "";
    TransactionStatusModel transactionStatusModel;
    private boolean ascending = true;
    SessionManager session;
    String tokenStr="";
    ProgressBar progressBar;
    Context context;
    Button wallet1,wallet2;
    Boolean calenderFlag = false;
    String transaction_type = "WALLETCASHOUT";


    public CashOutFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cashout, container, false);
        initView(rootView);

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        }
        else if(id == R.id.filterBar){
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }


    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);

                if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                    // filter recycler view when text is changed
                    cashoutReportRecyclerViewAdapter.getFilter().filter(s);
                    cashoutReportRecyclerViewAdapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }


    private void initView(View rootView) {
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setTitle("CashOut ");
        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressBar  = rootView.findViewById(R.id.progressV);

        //session = new Session(MicroAtmReportActivity.this);
        noData = rootView.findViewById ( R.id.noData );
        amount = rootView.findViewById ( R.id.amount );
        totalreport = rootView.findViewById ( R.id.totalreport );
        chooseDateRange = rootView.findViewById ( R.id.chooseDateRange );
        dateLayout = rootView.findViewById ( R.id.dateLayout );
        detailsLayout = rootView.findViewById ( R.id.detailsLayout );
        reportRecyclerView = rootView.findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager ( new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false ) );
        mActionsListener = new CashoutReportPresenter ( this );
        wallet1 = rootView.findViewById(R.id.wallet1);
        wallet2 = rootView.findViewById(R.id.wallet2);
        wallet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    wallet1.setTextColor(getResources().getColor(R.color.colorBluee));
                    wallet2.setTextColor(getResources().getColor(R.color.colorBlack));
                    transaction_type = "WALLETCASHOUT";
                    if(calenderFlag){
                        if(reportResponseArrayList!=null) {
                            if (!reportResponseArrayList.isEmpty()) {
                                reportResponseArrayList.clear();
                                if(cashoutReportRecyclerViewAdapter!=null) {
                                    cashoutReportRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                        callReportApi();
                    }else{
                        Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){

                }
            }
        });

        wallet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{


                    wallet2.setTextColor(getResources().getColor(R.color.colorBluee));
                    wallet1.setTextColor(getResources().getColor(R.color.colorBlack));
                    transaction_type = "WALLET2CASHOUT";
                    if(calenderFlag){
                        if(reportResponseArrayList!=null) {
                            if (!reportResponseArrayList.isEmpty()) {
                                reportResponseArrayList.clear();
                                if(cashoutReportRecyclerViewAdapter!=null) {
                                    cashoutReportRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                        callReportApi();
                    }else{
                        Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){

                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(CashOutFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate ( Calendar.getInstance () );
        dpd.setAutoHighlight ( true );
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + dayOfMonthEnd;
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+dayOfMonthEnd+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        // chooseDateRange.setText ( date );
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setSubtitle(date);


        if(date !=null && !date.matches ( "" ) && !date.matches ( "Choose Date Range for Reports" )) {

            callReportApi();
            calenderFlag = true;

        }else{
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }

    @Override
    public void reportsReady(ArrayList<CashoutReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText("Amt Tnx: ₹"+totalAmount);
    }

    @Override
    public void refreshDone(RefreshModel refreshModel) {

    }

    @Override
    public void showReports() {

        if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
            cashoutReportRecyclerViewAdapter = new CashoutReportRecyclerViewAdapter(getActivity(),reportResponseArrayList, new CashoutReportRecyclerViewAdapter.IMethodCaller() {
                @Override
                public void refreshMethod(CashoutReportModel microReportModel) {
                    /*clientId = String.valueOf(microReportModel.getId());
                    mActionsListener.refreshReports(tokenStr,String.valueOf(microReportModel.getAmountTransacted()),"statusEnquiry","mobile",String.valueOf(microReportModel.getId()));
               */ }
            });
            cashoutReportRecyclerViewAdapter.notifyDataSetChanged();
            reportRecyclerView.setAdapter(cashoutReportRecyclerViewAdapter);
            totalreport.setText("Entries: "+reportResponseArrayList.size());
        }else{
            totalreport.setText("Entries: 0");
            reportRecyclerView.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();

    }

    @Override
    public void checkAmount(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.amountrefersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.transaction_mode_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.transaction_type_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }
    //================
    public void callReportApi(){
        if (!(Util.getDateDiff ( new SimpleDateFormat( "yyyy-MM-dd" ),fromdate, todate ) > 10)) {
            if (Util.compareDates ( fromdate, todate ) == "1") {
                noData.setVisibility ( View.GONE );
                reportRecyclerView.setVisibility ( View.VISIBLE );
                mActionsListener.loadReports ( fromdate, Util.getNextDate (todate),tokenStr,transaction_type);
            } else if (Util.compareDates ( fromdate, todate ) == "2") {
                noData.setVisibility ( View.VISIBLE );
                reportRecyclerView.setVisibility ( View.GONE );
                Toast.makeText ( getActivity(), "Please select the Appropriate Dates", Toast.LENGTH_SHORT ).show ();
            } else if(Util.compareDates ( fromdate,todate) == "3" ) {
                noData.setVisibility ( View.GONE );
                reportRecyclerView.setVisibility ( View.VISIBLE );
                mActionsListener.loadReports ( fromdate,Util.getNextDate (todate),tokenStr,transaction_type);
            }
        } else {
            Toast.makeText ( getActivity(), "Please choose the date within 10 days from the current date ", Toast.LENGTH_LONG ).show ();
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }
}
