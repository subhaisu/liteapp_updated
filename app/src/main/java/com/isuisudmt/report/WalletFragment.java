package com.isuisudmt.report;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MATMActivity;
import com.isuisudmt.matm.MicroAtmReportActivity;
import com.isuisudmt.matm.MicroReportContract;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.MicroReportPresenter;
import com.isuisudmt.matm.MicroReportRecyclerViewAdapter;
import com.isuisudmt.matm.RefreshModel;
import com.isuisudmt.matm.TransactionStatusModel;
import com.isuisudmt.settings.SettingsActivity;
import com.isuisudmt.wallet.WalletReportContract;
import com.isuisudmt.wallet.WalletReportPresenter;
import com.isuisudmt.wallet.WalletReportRecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class WalletFragment extends Fragment implements WalletReportContract.View, DatePickerDialog.OnDateSetListener {

    private RecyclerView reportRecyclerView;
    private WalletReportPresenter mActionsListener;
    ArrayList<MicroReportModel> reportResponseArrayList ;
    LinearLayout dateLayout,detailsLayout;
    private WalletReportRecyclerViewAdapter microReportRecyclerViewAdapter;
    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    String fromdate = "";
    String todate = "";
    String clientId = "";
    TransactionStatusModel transactionStatusModel;
    private boolean ascending = true;
    SessionManager session;
    String tokenStr="",trans_type="";
    ProgressBar progressBar;
    Context context;
    Button wallet1,wallet2;
    Boolean calenderFlag = false;
    public static Boolean refreshmAtmReport = false;


    public WalletFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wallet, container, false);
        pref = getActivity().getSharedPreferences("DEFUALT", 0);
        editor = pref.edit();

        MicroReportRecyclerViewAdapter.transType="";

        initView(rootView);

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        }
        else if(id == R.id.filterBar){
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }


    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                try{
                    if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                        // filter recycler view when text is changed
                        microReportRecyclerViewAdapter.getFilter().filter(s);
                        microReportRecyclerViewAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){

                }
                return false;
            }
        });

    }


    private void initView(View rootView) {
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setTitle("Wallet Report");
        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressBar  = rootView.findViewById(R.id.progressV);

        //session = new Session(MicroAtmReportActivity.this);
        noData = rootView.findViewById ( R.id.noData );
        amount = rootView.findViewById ( R.id.amount );
        totalreport = rootView.findViewById ( R.id.totalreport );
        chooseDateRange = rootView.findViewById ( R.id.chooseDateRange );
        dateLayout = rootView.findViewById ( R.id.dateLayout );
        detailsLayout = rootView.findViewById ( R.id.detailsLayout );
        reportRecyclerView = rootView.findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager ( new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false ) );
        mActionsListener = new WalletReportPresenter ( this );
        wallet1 = rootView.findViewById(R.id.wallet1);
        wallet2 = rootView.findViewById(R.id.wallet2);


        wallet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trans_type="WALLET";//should be send in Api
                wallet1.setTextColor(getResources().getColor(R.color.colorBluee));
                wallet2.setTextColor(getResources().getColor(R.color.colorBlack));
                if(calenderFlag){
                    wallet1.setTextColor(getResources().getColor(R.color.colorBluee));
                    wallet2.setTextColor(getResources().getColor(R.color.colorBlack));
                    if(reportResponseArrayList!=null) {
                        reportResponseArrayList.clear();
                    }
                    if(microReportRecyclerViewAdapter!=null) {
                        microReportRecyclerViewAdapter.notifyDataSetChanged();
                    }

                    callReportApi(trans_type);
                }else{
                    Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        wallet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trans_type="WALLET2";//should be send in Api
                wallet2.setTextColor(getResources().getColor(R.color.colorBluee));
                wallet1.setTextColor(getResources().getColor(R.color.colorBlack));
                if(calenderFlag){
                    wallet2.setTextColor(getResources().getColor(R.color.colorBluee));
                    wallet1.setTextColor(getResources().getColor(R.color.colorBlack));
                    if(reportResponseArrayList!=null) {
                        reportResponseArrayList.clear();
                    }
                    if(microReportRecyclerViewAdapter!=null) {
                        microReportRecyclerViewAdapter.notifyDataSetChanged();
                    }

                    callReportApi(trans_type);
                }else{
                    Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();



        if(fromdate.length()!=0) {
            mActionsListener.loadReports(fromdate, Util.getNextDate(todate), tokenStr, "WALLET");
        }
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(WalletFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate ( Calendar.getInstance () );
        dpd.setAutoHighlight ( true );
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + dayOfMonthEnd;
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+dayOfMonthEnd+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        // chooseDateRange.setText ( date );
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setSubtitle(date);


        if(date !=null && !date.matches ( "" ) && !date.matches ( "Choose Date Range for Reports" )) {

            if (trans_type.equals("")){
                callReportApi("WALLET");
            }
            else {
                callReportApi(trans_type);
            }
            calenderFlag = true;

        }else{
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }

    @Override
    public void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText("Amt Tnx: ₹"+totalAmount);
    }



    @Override
    public void showReports() {

        if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);

            microReportRecyclerViewAdapter = new WalletReportRecyclerViewAdapter(getActivity(),reportResponseArrayList,trans_type, new WalletReportRecyclerViewAdapter.IMethodCaller() {
                @Override
                public void refreshMethod(MicroReportModel microReportModel) {

                }
            });
            reportRecyclerView.setAdapter(microReportRecyclerViewAdapter);
            microReportRecyclerViewAdapter.notifyDataSetChanged();
            totalreport.setText("Entries: "+reportResponseArrayList.size());
        }else{
            reportRecyclerView.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
            totalreport.setText("Entries: "+ 0);

        }

    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
    }

    @Override
    public void checkAmount(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.amountrefersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.transaction_mode_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.transaction_type_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }



    //================
    public void callReportApi(String trans_tpye) {
        if (!(Util.getDateDiff(new SimpleDateFormat("yyyy-MM-dd"), fromdate, todate) > 10)) {
            if (!fromdate.equalsIgnoreCase("") || !todate.equalsIgnoreCase("")) {

                if (Util.compareDates(fromdate, todate) == "1") {
                    noData.setVisibility(View.GONE);
                    reportRecyclerView.setVisibility(View.VISIBLE);
                    mActionsListener.loadReports(fromdate, Util.getNextDate(todate), tokenStr, trans_tpye);
                } else if (Util.compareDates(fromdate, todate) == "2") {
                    noData.setVisibility(View.VISIBLE);
                    reportRecyclerView.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Please select the Appropriate Dates", Toast.LENGTH_SHORT).show();
                } else if (Util.compareDates(fromdate, todate) == "3") {
                    noData.setVisibility(View.GONE);
                    reportRecyclerView.setVisibility(View.VISIBLE);
                    mActionsListener.loadReports(fromdate, Util.getNextDate(todate), tokenStr, trans_tpye);
                }
            }
            else {
//                Toast.makeText ( getActivity(), "Please choose the date !", Toast.LENGTH_LONG ).show ();
                noData.setVisibility ( View.VISIBLE );
                reportRecyclerView.setVisibility ( View.GONE );
            }
        }


        else {
            Toast.makeText ( getActivity(), "Please choose the date within 10 days from the current date ", Toast.LENGTH_LONG ).show ();
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }



}
