package com.isuisudmt.wallet;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MicroReportModel;

import java.util.ArrayList;
import java.util.List;

import static com.isuisudmt.report.MatmFragment.refreshmAtmReport;

public class WalletReportRecyclerViewAdapter extends RecyclerView.Adapter<WalletReportRecyclerViewAdapter.ReportViewhOlder> implements Filterable {

private List<MicroReportModel> reportModelListFiltered;
private List<MicroReportModel> reportModels;
    private IMethodCaller listener;
    private int lastSelectedPosition = -1;
    Context context;
    public static String transType="";


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    reportModelListFiltered = reportModels;
                } else {
                    List<MicroReportModel> filteredList = new ArrayList<>();
                    for (MicroReportModel row : reportModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.getUserName().toLowerCase().contains(charString.toLowerCase())||
                                        row.getStatus().toLowerCase().contains(charString.toLowerCase()) ||
                                        row.getTransactionMode().toLowerCase().contains(charString.toLowerCase())||
                                        String.valueOf(row.getId()).contains(charString.toLowerCase())||
                                        row.getOperationPerformed().toLowerCase().contains(charString.toLowerCase())

                        ) {
                            filteredList.add(row);
                        }
                    }

                    reportModelListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                reportModelListFiltered = (ArrayList<MicroReportModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ReportViewhOlder extends RecyclerView.ViewHolder {

   /* public ImageView refreshImage;
    public TextView updatedDateTextView;
    public TextView statusTextView,createdDateTextView,apiDateTextView;
    public TextView rrnTextView,transactionTypeTextView,IdTextView,apiCommentTextView,amountTransactedTextView,userNameReportTextView,opertionPerformedTextView,cardNumberTextView;

*/
   public ImageView refreshImage;
    public TextView openingBal,closingBal,dateTime,statusTextView,tnxId,tnxType,userName,createdDate,operationPerformed,flag,amount;
    LinearLayout mainView,topLayout,buttomlayout;


    public ReportViewhOlder(View view) {
        super ( view );

      /*  refreshImage= view.findViewById ( R.id.refreshImage );
        refreshImage.setVisibility(View.GONE);
        userNameReportTextView= view.findViewById ( R.id.userNameReportTextView );
        updatedDateTextView= view.findViewById ( R.id.updatedDateTextView );
        statusTextView= view.findViewById ( R.id.statusTextView );
        rrnTextView= view.findViewById ( R.id.rrnTextView );
        transactionTypeTextView= view.findViewById ( R.id.transactionTypeTextView );
        IdTextView= view.findViewById ( R.id.IdTextView );
        apiCommentTextView= view.findViewById ( R.id.apiCommentTextView );
        amountTransactedTextView= view.findViewById ( R.id.amountTransactedTextView );
        opertionPerformedTextView= view.findViewById ( R.id.opertionPerformedTextView );
        cardNumberTextView= view.findViewById ( R.id.cardNumberTextView );
        createdDateTextView= view.findViewById ( R.id.createdDateTextView );
        apiDateTextView= view.findViewById ( R.id.apiDateTextView );*/
        refreshImage= view.findViewById ( R.id.refreshImage );
        refreshImage.setVisibility(View.GONE);
        dateTime = view.findViewById(R.id.dateTime);
        amount = view.findViewById(R.id.amount);
        statusTextView = view.findViewById(R.id.statusTextView);
        tnxId = view.findViewById(R.id.tnxId);
        tnxType = view.findViewById(R.id.tnxType);
        userName = view.findViewById(R.id.userName);
        createdDate = view.findViewById(R.id.createdDate);
        openingBal = view.findViewById(R.id.openingBal);
        closingBal = view.findViewById(R.id.closingBal);
        flag =view.findViewById(R.id.flag);
        topLayout = view.findViewById(R.id.topLayout);
        buttomlayout = view.findViewById(R.id.buttomlayout);
        mainView =view.findViewById(R.id.mainView);

        topLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttomlayout.setVisibility(View.VISIBLE);
                //mainView.set
                if(flag.getText().toString().equalsIgnoreCase("false")){
                    buttomlayout.setVisibility(View.VISIBLE);
                    flag.setText("true");
                    mainView.setBackgroundColor(Color.parseColor("#DCE0E0"));
                }else{
                    mainView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                    buttomlayout.setVisibility(View.GONE);
                    flag.setText("false");
                }
            }
        });

        refreshImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("test","wallet Button : ButtonClicked");
                lastSelectedPosition = getAdapterPosition();
                refreshmAtmReport=true;
                listener.refreshMethod(reportModelListFiltered.get(lastSelectedPosition));
                notifyDataSetChanged();
            }
        });


    }
}

    public WalletReportRecyclerViewAdapter(Context context, List<MicroReportModel> reportModels, String transType, IMethodCaller listener) {
        this.reportModels = reportModels;
        this.reportModelListFiltered = reportModels;
        this.listener =listener;
        this.context = context;
        this.transType=transType;

    }

    public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.micro_report_row, parent, false);

        return new ReportViewhOlder(itemView);
    }

    public void onBindViewHolder(ReportViewhOlder holder, int position) {
        MicroReportModel reportModel = reportModelListFiltered.get(position);

        if(reportModel.getStatus().equalsIgnoreCase("INITIATED")  ){
            holder.refreshImage.setVisibility(View.GONE);
        }else{
            holder.refreshImage.setVisibility(View.GONE);
        }

        if(reportModel.getUserName() != null && !reportModel.getUserName().matches("")){
            holder.userName.setVisibility(View.VISIBLE);
            holder.userName.setText(reportModel.getUserName());
        }else{
            holder.userName.setVisibility(View.GONE);
        }

        if(reportModel.getStatus() != null && !reportModel.getStatus().matches("")){
            holder.statusTextView.setVisibility(View.VISIBLE);
            if(reportModel.getStatus().equalsIgnoreCase("SUCCESS")){
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
            }else{
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            holder.statusTextView.setText(reportModel.getStatus());
        }else{
            holder.statusTextView.setVisibility(View.GONE);
        }


        if(reportModel.getId() != null ||!reportModel.getId().equalsIgnoreCase("")){
            holder.tnxId.setVisibility(View.VISIBLE);
            holder.tnxId.setText("Txn ID: "+reportModel.getId());
        }else{
            holder.tnxId.setVisibility(View.GONE);
        }
        if(reportModel.getTransactionMode() != null){
            holder.tnxType.setVisibility(View.VISIBLE);
            holder.tnxType.setText("Txn Type: "+reportModel.getTransactionMode());
        }else{
            holder.tnxType.setVisibility(View.GONE);
        }

        if(reportModel.getAmountTransacted() != null){
            holder.amount.setVisibility(View.VISIBLE);
            holder.amount.setText("₹ "+reportModel.getAmountTransacted());
        }else{
            holder.amount.setVisibility(View.GONE);
        }
        if(reportModel.getCreatedDate() != null && !reportModel.getCreatedDate().matches("")){
            holder.dateTime.setVisibility(View.VISIBLE);
            holder.dateTime.setText( "Date: "+Util.getDateFromTime(Long.parseLong(reportModel.getCreatedDate())));
        }else{
            holder.dateTime.setVisibility(View.GONE);
        }
        holder.openingBal.setText(reportModel.getPreviousAmount().toString());
        holder.closingBal.setText(reportModel.getBalanceAmount().toString());

    }

    public int getItemCount() {
        return reportModelListFiltered.size();
    }

    public interface IMethodCaller{
        void refreshMethod(MicroReportModel microReportModel);
    }

}
