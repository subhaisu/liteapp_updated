package com.isuisudmt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.matm.MATMActivity;
import com.isuisudmt.utility.ConnectionDetector;
import com.isuisudmt.utils.ForceUpdateAsync;

import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.isuisudmt.utils.Constants.APP_UNIQUE_ID;

public class SplashActivity extends AppCompatActivity {

    Boolean navigationflag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionDetector cd = new ConnectionDetector(
                SplashActivity.this);
        if (cd.isConnectingToInternet()) {
            forceUpdate();
        }else {
            Toast.makeText(SplashActivity.this,"No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    public void forceUpdate(){
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;

        try {
            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String currentVersion = packageInfo.versionName;
        try {
        new ForceUpdateAsync(currentVersion, SplashActivity.this, new ForceUpdateAsync.AsyncListener() {
            @Override
            public void doStuff(String latestVersion, String currentVersion) {
                if (latestVersion != null && !latestVersion.equalsIgnoreCase("Varies with device")) {
                    if (!currentVersion.equalsIgnoreCase ( latestVersion )) {

                        showForceUpdateDialog ( latestVersion );

                    } else {

                        checkVersionCode(latestVersion);
                       /* if(navigationflag==false) {
                            Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(in);
                            finish();
                            navigationflag = true;
                        }*/
                    }
                } else {
                    checkVersionCode(latestVersion);
                    /*if(navigationflag==false) {
                        Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(in);
                        finish();
                        navigationflag = true;
                    }*/
                }
            }
        }).execute();
        }catch (Exception e){

        }
    }

    private void updateAppVersionName(String version_name,String package_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> map = new HashMap<>();
        if(version_name==null){
            version_name="0.0.1";
        }
        map.put("VersionName", version_name);
        if(!status) {
            map.put("UniqueId", APP_UNIQUE_ID);
            db.collection("CoreApp_Version_Management").document(getPackageName())
                    .set(map)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "DocumentSnapshot successfully written!");
                            if(navigationflag==false) {
                                Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                                startActivity(in);
                                finish();
                                navigationflag = true;
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error writing document", e);
                            if(navigationflag==false) {
                                Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                                startActivity(in);
                                finish();
                                navigationflag = true;
                            }
                        }
                    });
        }
        else {
            db.collection("CoreApp_Version_Management").document(getPackageName())
                    .update(map)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "DocumentSnapshot successfully written!");
                            if(navigationflag==false) {
                                Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                                startActivity(in);
                                finish();
                                navigationflag = true;
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error writing document", e);
                            if(navigationflag==false) {
                                Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                                startActivity(in);
                                finish();
                                navigationflag = true;
                            }
                        }
                    });
        }

    }
    private void checkVersionCode(String latestVersion){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Version_Management").document(getPackageName());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("UniqueId")) {

                                String value = document.getData().get("UniqueId").toString();

                                if(value.equalsIgnoreCase(APP_UNIQUE_ID)){

                                    updateAppVersionName(latestVersion,getPackageName(),true);

                                }else{
                                    showStatusFailure("Unauthorized, please call or email helpdesk to continue service.");
                                    // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
                        updateAppVersionName(latestVersion,getPackageName(),false);

                    }
                } else {
                    updateAppVersionName(latestVersion,getPackageName(),false);
                }
            }
        });
    }

    private void showStatusFailure(final String statusDesc){


        AlertDialog.Builder builder1 = new AlertDialog.Builder(SplashActivity.this);
        builder1.setMessage(statusDesc);
        builder1.setTitle("Alert");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();

    }
    public void showForceUpdateDialog(final String latestVersion){

        if(!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(SplashActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    String message = "A new version of this app is available. Please update to version " + latestVersion + " " + getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle(getResources().getString(R.string.youAreNotUpdatedTitle))
                            .setMessage(message)
                            .setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                                    dialog.dismiss();
                                }
                            })
                            /*.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                                    dialog.dismiss();
                                    updateDeviceList ();
                                }
                            })*/
                            .show();
                }
            });
        }

    }
}
