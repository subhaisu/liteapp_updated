package com.isuisudmt.aeps;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ReportActivity extends AppCompatActivity implements ReportContract.View, DatePickerDialog.OnDateSetListener {

    private RecyclerView reportRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ReportPresenter mActionsListener;
    ArrayList<ReportModel> reportResponseArrayList ;
    LinearLayout dateLayout,detailsLayout;

    private ReportRecyclerViewAdapter reportRecyclerviewAdapter;
    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    private static final String TAG = ReportActivity.class.getSimpleName ();
   // LoadingView loadingView;
   // Session session;
   // SearchView searchView;
    private boolean ascending = true;

    SessionManager session;
    String tokenStr="";
    ProgressDialog pd;
    String aepstype = "AEPSFUNT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_report);

        setToolbar ();

       // session = new Session(ReportActivity.this);
        pd = new ProgressDialog(ReportActivity.this);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        noData = findViewById ( R.id.noData );
        amount = findViewById ( R.id.amount );
        totalreport = findViewById ( R.id.totalreport );
        detailsLayout = findViewById ( R.id.detailsLayout );
        chooseDateRange = findViewById ( R.id.chooseDateRange );
        dateLayout = findViewById ( R.id.dateLayout );
        reportRecyclerView = findViewById ( R.id.reportRecyclerView );
        mLayoutManager = new LinearLayoutManager(ReportActivity.this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager(mLayoutManager);
        mActionsListener = new ReportPresenter ( ReportActivity.this );

        dateLayout.setOnClickListener(new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(ReportActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
                dpd.setMaxDate ( Calendar.getInstance () );
                dpd.setAutoHighlight ( true );
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            return false;
        }
        return false;
    }

    private void setToolbar() {
        // Inflate the layout for this fragment
        Toolbar mToolbar = findViewById ( R.id.toolbar );
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle (getResources().getString(R.string.aeps_report_title) );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //What to do on back clicked
            }
        });
    }


    @Override
    public void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText(getResources().getString(R.string.toatlamountacitvity)+totalAmount);

    }

    @Override
    public void AEPS2reportsReady(ArrayList<AEPS2ReportModel> reportModelArrayList, String totalAmount) {

    }

    @Override
    public void showReports() {

        if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
            reportRecyclerviewAdapter = new ReportRecyclerViewAdapter(reportResponseArrayList,ReportActivity.this);
            reportRecyclerView.setAdapter(reportRecyclerviewAdapter);
            totalreport.setText("Found "+reportResponseArrayList.size()+"/"+reportResponseArrayList.size()+" Entries");
        }else{
            reportRecyclerView.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoader() {
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    public void hideLoader() {
        pd.dismiss();

    }

    @Override
    public void emptyDates() {
        Toast.makeText(ReportActivity.this,getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        String fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + dayOfMonthEnd;
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+dayOfMonthEnd+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        chooseDateRange.setText ( date );

        if(chooseDateRange.getText () !=null && !chooseDateRange.getText ().toString ().matches ( "" ) && !chooseDateRange.getText ().toString ().trim ().matches ( "Choose Date Range for Reports" )) {

            if (!(Util.getDateDiff ( new SimpleDateFormat ( "yyyy-MM-dd" ),fromdate, todate ) > 10)) {
                if (Util.compareDates ( fromdate, todate ) == "1") {
                    noData.setVisibility ( View.GONE );
                    reportRecyclerView.setVisibility ( View.VISIBLE );
                    mActionsListener.loadReports ( fromdate, Util.getNextDate (todate), tokenStr,aepstype);
                } else if (Util.compareDates ( fromdate, todate ) == "2") {
                    noData.setVisibility ( View.VISIBLE );
                    reportRecyclerView.setVisibility ( View.GONE );
                    Toast.makeText ( ReportActivity.this, getResources().getString(R.string.appropriate_dates), Toast.LENGTH_SHORT ).show ();
                } else if(Util.compareDates ( fromdate,todate) == "3" ) {
                    noData.setVisibility ( View.GONE );
                    reportRecyclerView.setVisibility ( View.VISIBLE );
                    mActionsListener.loadReports ( fromdate,Util.getNextDate (todate), tokenStr,aepstype);
                }
            } else {
                Toast.makeText ( ReportActivity.this, getResources().getString(R.string.dates_limit), Toast.LENGTH_LONG ).show ();
                noData.setVisibility ( View.VISIBLE );
                reportRecyclerView.setVisibility ( View.GONE );
            }
        }else{
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
            // Associate searchable configuration with the SearchView
           // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
           /* searchView = (SearchView) menu.findItem(R.id.action_search)
                    .getActionView();
            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
            AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

            try {
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
            } catch (Exception e) {
            }*/

           // searchView.setMaxWidth(Integer.MAX_VALUE);

            // listening to search query text change
          /*  searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    // vendorPriceListAdapter.getFilter().filter(query);
                    return false;
                }*/

                /*@Override
                public boolean onQueryTextChange(String query) {
                    if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                        // filter recycler view when text is changed
                    reportRecyclerviewAdapter.getFilter().filter(query);
                    }else{
                        Toast.makeText(ReportActivity.this,getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                    }
                    return false;
                }
            });*/


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_calender) {

            callCalenderFunction();
            return true;
        }
        /*else if(id == R.id.action_sort){
            sortData(ascending);
            ascending = !ascending;
        }*/

        return super.onOptionsItemSelected(item);
    }


    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(ReportActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate ( Calendar.getInstance () );
        dpd.setAutoHighlight ( true );
    }


   /* private void sortData(boolean asc)
    {
        //SORT ARRAY ASCENDING AND DESCENDING
        if (asc) {
            Collections.reverse(reportResponseArrayList);
        }
        else {
            Collections.reverse(reportResponseArrayList);
        }
        //ADAPTER
        reportRecyclerviewAdapter = new ReportRecyclerViewAdapter(reportResponseArrayList);
        reportRecyclerView.setAdapter(reportRecyclerviewAdapter);

    }*/
}
