package com.isuisudmt.aeps;

import java.util.ArrayList;

public class ReportResponse {
    ArrayList<ReportModel> AepsreportList =new ArrayList<>();

    public ArrayList<ReportModel> getAepsreportList() {
        return AepsreportList;
    }

    public void setAepsreportList(ArrayList<ReportModel> aepsreportList) {
        AepsreportList = aepsreportList;
    }
}
