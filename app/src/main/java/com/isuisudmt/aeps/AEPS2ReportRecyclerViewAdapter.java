package com.isuisudmt.aeps;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;

import java.util.ArrayList;
import java.util.List;

public class AEPS2ReportRecyclerViewAdapter extends RecyclerView.Adapter<AEPS2ReportRecyclerViewAdapter.ReportViewhOlder> implements Filterable {

    private List<AEPS2ReportModel> reportModelListFiltered;
        private List<AEPS2ReportModel> reportModels;
    Context context;
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        reportModelListFiltered = reportModels;
                    } else {
                        List<AEPS2ReportModel> filteredList = new ArrayList<> ();
                        for (AEPS2ReportModel row : reportModels) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getApiComment ().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getApiTid().toLowerCase().contains(charString.toLowerCase()) ||

                                    String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getReferenceNo().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getTransactionMode().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getUserName().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                        reportModelListFiltered = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = reportModelListFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    reportModelListFiltered = (ArrayList<AEPS2ReportModel>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }
        public static class ReportViewhOlder extends RecyclerView.ViewHolder {

            TextView dateTime,statusTextView,bankName,rrnTextView,amount,tnxType,userName,createdDate,tranID,trackID,openingBal,closingBal,flag;
            LinearLayout topLayout,buttomlayout,mainView;

            public ReportViewhOlder(View view) {
                super ( view );
                dateTime = view.findViewById(R.id.dateTime);
                statusTextView = view.findViewById(R.id.statusTextView);
               // bankName = view.findViewById(R.id.bankName);
                amount = view.findViewById(R.id.amount);
                tnxType = view.findViewById(R.id.tnxType);
                userName = view.findViewById(R.id.userName);
                rrnTextView = view.findViewById(R.id.rrnTextView);
                tranID = view.findViewById(R.id.tranID);
                trackID = view.findViewById(R.id.trackID);
                openingBal = view.findViewById(R.id.openingBal);
                closingBal = view.findViewById(R.id.closingBal);
                flag = view.findViewById(R.id.flag);

                topLayout = view.findViewById(R.id.topLayout);
                buttomlayout = view.findViewById(R.id.buttomlayout);
                mainView = view.findViewById(R.id.mainView);

                topLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        buttomlayout.setVisibility(View.VISIBLE);
                        //mainView.set
                        if(flag.getText().toString().equalsIgnoreCase("false")){
                            buttomlayout.setVisibility(View.VISIBLE);
                            flag.setText("true");
                            mainView.setBackgroundColor(Color.parseColor("#DCE0E0"));
                        }else{
                            mainView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                            buttomlayout.setVisibility(View.GONE);
                            flag.setText("false");
                        }
                    }
                });

            }


        }



        public AEPS2ReportRecyclerViewAdapter(List<AEPS2ReportModel> reportModels,Context context) {
            this.reportModels = reportModels;
            this.reportModelListFiltered = reportModels;
            this.context=context;
        }



        public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.report_row, parent, false);

            return new ReportViewhOlder(itemView);
        }

        public void onBindViewHolder(ReportViewhOlder holder, int position) {

            try {

                AEPS2ReportModel reportModel = reportModelListFiltered.get(position);
                //holder.bankName.setText("Bank Name : " + reportModel.getBankName ());
                if(reportModel.getOperationPerformed().contains("AEPS_CASH_WITHDRAWAL")){
                    holder.tnxType.setText("Txn Type : " + "WITHDRAW");
                }else if(reportModel.getOperationPerformed().contains("AEPS_BALANCE_ENQUIRY")){
                    holder.tnxType.setText("Txn Type : " + "ENQUIRY");
                }else{
                    holder.tnxType.setText("Txn Type : " + "MINI STATEMENT");
                }
                holder.trackID.setText(reportModel.getApiTid ());
                //holder.tnxType.setVisibility(View.GONE);
                holder.tranID.setText(""+reportModel.getId ());
                holder.userName.setText(reportModel.getUserName ());
                holder.openingBal.setText(reportModel.getPreviousAmount().toString());
                holder.closingBal.setText(reportModel.getBalanceAmount().toString());
                holder.amount.setText("Amount : "+reportModel.getAmountTransacted());
                holder.rrnTextView.setText(reportModel.getReferenceNo());
                holder.dateTime.setText("Date: "+Util.getDateFromTime(reportModel.getUpdatedDate()));

//                holder.statusTextView.setText(reportModel.getStatus().toUpperCase());

                if(reportModel.getStatus() != null && !reportModel.getStatus().matches("")){
                    holder.statusTextView.setVisibility(View.VISIBLE);
                    if(reportModel.getStatus().equalsIgnoreCase("SUCCESS")){
                        holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
                    }else{
                        holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red));
                    }
                    holder.statusTextView.setText(reportModel.getStatus().toUpperCase());
                }else{
                    holder.statusTextView.setVisibility(View.GONE);
                }

//             holder.statusTextView.setText(reportModel.getStatus().toUpperCase());



            }catch (Exception e){

            }


        }

        public int getItemCount() {
            return reportModelListFiltered.size();
        }

    }
