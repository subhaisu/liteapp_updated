package com.isuisudmt;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.aeps.AEPSActivity;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.isuisudmt.changePassword.ChangePasswordActivity;
import com.isuisudmt.cms.CMSActivity;
import com.isuisudmt.dmt.TransferFragment;
import com.isuisudmt.insurance.Insurance;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.matm.MATMActivity;
import com.isuisudmt.report.ReportDashboardActivity;
import com.isuisudmt.settings.SettingsActivity;
import com.matm.matmsdk.Utils.SdkConstants;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static com.isuisudmt.BuildConfig.GET_USER_ROLE;
import static com.isuisudmt.Constants.session_time;
import static com.isuisudmt.utils.Constants.APP_UNIQUE_ID;

public class MainActivity extends AppCompatActivity implements HomeContract.View, NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;

    // private TextView aeps_view,micro_view;
    private EditText amnt_txt, loan_id;
    private Button aeps_submit_btn, matm_submit_btn, btnPair, upi_btn;
    RadioGroup rg_trans_type;
    RadioButton rb_cw, rb_be;
    String strTransType = "";
    private Handler handler = new Handler();


    TextView das_user_name, nav_user_name, view_details, das_user_balance, das_user_name2, das_user_balance2, app_name_txt;
    ImageView menu_nav, go_profile, con_menu, con_notification, add_money;

    //ViewPager vpPager;
    //TabsPagerAdapter myAdapter;

    LinearLayout nav_insurance, nav_cms, nav_wallet;
    CardView _nav_view;

    SessionManager session;
    HomePresenter homePresenter;

    String _token = "", _admin = "";

    DrawerLayout drawer;
    NavigationView navigationView;

    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";

    SharedPreferences sharedpreferences;
    BottomNavigationView navigation;
    //FlexibleBottomNavigationView flexibleBottomNavigationView;
    ImageView firstDotImageView, secondDotImageView, reload, reload2;
    ProgressBar progressBar, progressBar2;
    CircleImageView customer_image, customer2_image;
    String _userName = "", image_url = "", _adminName = "";
    CardView dmtCrdView, aepsCrdView, matmCrdView;
    Handler timer_handler;
    boolean session_logout = false;
    ProgressDialog pd;
    SharePreferenceClass sharePreferenceClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        app_name_txt = findViewById(R.id.app_name_txt);
        app_name_txt.setText(getApplicationName(MainActivity.this));
        sharePreferenceClass = new SharePreferenceClass(MainActivity.this);

//        firebaseAnalytics=FirebaseAnalytics.getInstance(MainActivity.this);

        initView();
        loadEvent();

    }

    private void loadEvent() {

        //myAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        //vpPager.setAdapter(myAdapter);

        menu_nav.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });


        view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                drawer.closeDrawers();
            }
        });

        nav_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Insurance.class));
            }
        });

        nav_cms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CMSActivity.class));
            }
        });

        nav_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AddMoneyActivity.class));
            }
        });

       /* vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0) {
                    Log.i("position 0 ::", "" + position);
                    firstDotImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.circle));
                    secondDotImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.circle_radio));
                } else if (position == 1) {
                    Log.i("position 1 ::", "" + position);
                    firstDotImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.circle_radio));
                    secondDotImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.circle));
                } else {
                    Log.i("position ::", "" + position);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        firstDotImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpPager.setCurrentItem(0);
            }
        });

        secondDotImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpPager.setCurrentItem(1);
            }
        });*/

        con_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
            }
        });


        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reload.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgressTintList(ColorStateList.valueOf(Color.WHITE));

                homePresenter.getHomeDashboardResponse("https://itpl.iserveu.tech");
            }
        });

        reload2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reload2.setVisibility(View.GONE);
                progressBar2.setVisibility(View.VISIBLE);
                progressBar2.setProgressTintList(ColorStateList.valueOf(Color.WHITE));
                getWallet2Balance();

            }
        });

        // CardView dmtCrdView,aepsCrdView,matmCrdView;

        dmtCrdView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.user_feature_array != null) {
                    if (Constants.user_feature_array.contains("30")) {
                        Intent navigation_dmt = new Intent(MainActivity.this, TransferFragment.class);
                        startActivity(navigation_dmt);
                    } else {
                        Toast.makeText(MainActivity.this, "DMT Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        aepsCrdView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.user_feature_array != null) {
                    if (Constants.user_feature_array.contains("27") || Constants.user_feature_array.contains("47")) {
                        Intent intent = new Intent(MainActivity.this, AEPSActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "AEPS Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        matmCrdView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if(timer_handler!=null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }*/
                if (Constants.user_feature_array != null) {
                    if (Constants.user_feature_array.contains("33") || Constants.user_feature_array.contains("48")) {
                        Intent microATM = new Intent(MainActivity.this, MATMActivity.class);
                        startActivity(microATM);
                    } else {
                        Toast.makeText(MainActivity.this, "MATM Feature not available, please try after sometimes", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);
        _userName = user.get(SessionManager.userName);

        homePresenter = new HomePresenter(MainActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //startTimer();

        homePresenter.getHomeDashboardResponse("https://itpl.iserveu.tech");
    }

    private void initView() {
        pd = new ProgressDialog(MainActivity.this);
        dmtCrdView = findViewById(R.id.dmtCrdView);
        aepsCrdView = findViewById(R.id.aepsCrdView);
        matmCrdView = findViewById(R.id.matmCrdView);

        navigation = findViewById(R.id.navigation);
        firstDotImageView = findViewById(R.id.firstDotImageView);
        secondDotImageView = findViewById(R.id.secondDotImageView);
        reload = findViewById(R.id.reload);
        reload2 = findViewById(R.id.reload2);
        progressBar = findViewById(R.id.progressBar);
        progressBar2 = findViewById(R.id.progressBar2);
        con_notification = findViewById(R.id.con_notification);
        customer_image = findViewById(R.id.customer_image);
        customer2_image = findViewById(R.id.customer2_image);
        navigation.setOnNavigationItemSelectedListener(MainActivity.this);

        // flexibleBottomNavigationView = findViewById(R.id.flexibleBottomNavigationView);
        // flexibleBottomNavigationView.inflateMenu(R.menu.navigation);

        // When you want to stop animation
        // flexibleBottomNavigationView.enableShiftMode(false);
        // flexibleBottomNavigationView.setOnNavigationItemSelectedListener(this);

        menu_nav = findViewById(R.id.menu_nav);
        drawer = findViewById(R.id.drawer_layout);

        // vpPager = findViewById(R.id.viewpager);

        nav_insurance = findViewById(R.id.insurance);
        nav_cms = findViewById(R.id.cms);
        nav_wallet = findViewById(R.id.wallet);


        das_user_name = findViewById(R.id.das_user_name);
        das_user_balance = findViewById(R.id.das_user_balance);
        das_user_name2 = findViewById(R.id.das_user_name2);
        das_user_balance2 = findViewById(R.id.das_user_balance2);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(MainActivity.this);
        View headerview = navigationView.getHeaderView(0);

        nav_user_name = headerview.findViewById(R.id.nav_user_name);
        view_details = headerview.findViewById(R.id.view_details);
    }


    private void loadDashboardFeature(String base64) {
        loadUserDashboard(base64);
    }

    private void loadUserDashboard(String base64) {

        if (pd != null) {
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        StringRequest request = new StringRequest(Request.Method.GET, base64, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (!response.equals(null)) {

                    // Log.e("Your Array Response", response);
                   /* String aeps = "27";
                    String dmt = "30";
                    String matmFeatureCode = "33";
                    String aeps2FeatureCode = "47";
                    String matm2FeatureCode = "48";
                    String wallet2 = "41";
                    String wallet3 = "42";*/

                    progressBar.setVisibility(View.GONE);

                    reload.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = null;

                    try {
                        jsonObject = new JSONObject(response);
                        JSONObject _userInfo = jsonObject.optJSONObject("userInfo");
                        _userName = _userInfo.optString("userName");
                        String _userType = _userInfo.optString("userType");
                        String _userBalance = _userInfo.optString("userBalance");
                        _adminName = _userInfo.optString("adminName");
                        String _mposNumber = _userInfo.optString("mposNumber");
                        String _promotionalMessage = _userInfo.optString("promotionalMessage");

                        String _userBrand = _userInfo.optString("userBrand");
                        String _userProfile = String.valueOf(_userInfo.getJSONObject("userProfile"));
                        String _userFeature = String.valueOf(_userInfo.getJSONArray("userFeature"));


                        if(_userInfo.has("userBrand")){

                            if (!_userBrand.equals("")){
                                try {
                                    JSONObject jsonObject1=  new JSONObject(_userInfo.getString("userBrand"));
                                    String brand = jsonObject1.getString("brand");
                                    com.isuisudmt.utils.Constants.BRAND_NAME = brand;
                                }catch (Exception e){

                                }

                            }
                            else {
                                com.isuisudmt.utils.Constants.BRAND_NAME = "";
                            }


                        }else{
                            com.isuisudmt.utils.Constants.BRAND_NAME = "";
                        }

                        JSONObject obj = new JSONObject(_userProfile);
                        String fname = obj.getString("firstName");
                        String lname = obj.getString("lastName");
                        String mobileNo = obj.getString("mobileNumber");
                        String shopName = obj.getString("shopName");
                        com.isuisudmt.utils.Constants.SHOP_NAME =shopName;
                        com.isuisudmt.utils.Constants.USER_MOBILE_NO = mobileNo;

                        String profileName = fname + " " + lname;

                        session.createUserSession(_userName, _userType, _userBalance, _adminName, _mposNumber, _userName, profileName, mobileNo);

                        das_user_name.setText(profileName);
                        das_user_balance.setText("₹ " + _userBalance);

                        nav_user_name.setText(profileName);

                        ArrayList<String> feature_array = new ArrayList<>();
                        JSONArray jsonArray = _userInfo.getJSONArray("userFeature");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            feature_array.add(jsonObject1.getString("id"));
                        }

                        Constants.user_feature_array = feature_array;
                       /* if(_userName!=null) {
                            checkSessionExistance(_userName,true);
                        }*/

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                loadProfileImage();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                        reload.setVisibility(View.VISIBLE);
                        if (!isFinishing() && !isDestroyed()) {
                            if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                                pd.cancel();
                            }
                        }
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    reload.setVisibility(View.VISIBLE);
                    if (!isFinishing() && !isDestroyed()) {
                        if (pd != null && pd.isShowing()) {
                            pd.dismiss();
                            pd.cancel();
                        }
                    }
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                progressBar.setVisibility(View.GONE);
                reload.setVisibility(View.VISIBLE);
                if (!isFinishing() && !isDestroyed()) {
                    if (pd != null && pd.isShowing()) {
                        pd.dismiss();
                        pd.cancel();
                    }
                    sessionAlert(MainActivity.this);
                }
                }catch (Exception e){

                }
            }
        }) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }

            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.navigation_home:
                //startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            case R.id.navigation_aeps:
                if (Constants.user_feature_array != null) {
                    if (Constants.user_feature_array.contains("27") || Constants.user_feature_array.contains("47")) {
                        Intent intent = new Intent(MainActivity.this, AEPSActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(this, "AEPS Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.navigation_matm:
                if (Constants.user_feature_array != null) {
                    if (Constants.user_feature_array.contains("33") || Constants.user_feature_array.contains("48")) {
                        Intent microATM = new Intent(MainActivity.this, MATMActivity.class);
                        startActivity(microATM);
                    } else {
                        Toast.makeText(this, "MATM Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.navigation_dmt:
                if (Constants.user_feature_array != null) {
                    if (Constants.user_feature_array.contains("30")) {
                        Intent navigation_dmt = new Intent(MainActivity.this, TransferFragment.class);
                        startActivity(navigation_dmt);
                    } else {
                        Toast.makeText(this, "DMT Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            case R.id.setting:

                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                finish();
                break;
            case R.id.report:

                Intent _intent = new Intent(MainActivity.this, ReportDashboardActivity.class);
                startActivity(_intent);
                break;
            case R.id.cashout:

                Intent __intent = new Intent(MainActivity.this, SelectCashoutWalletOptionActivity.class);
                startActivity(__intent);
                break;
           /* case R.id.report_matm:
                Intent report_matm = new Intent(MainActivity.this, MicroAtmReportActivity.class);
                startActivity(report_matm);
                break;
            case R.id.report_dmt:
//                Intent report_dmt = new Intent(MainActivity.this, FundTransferReport.class);
//                startActivity(report_dmt);
                break;*/
            case R.id.change_password:
                Intent pwdIntent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                startActivity(pwdIntent);

                break;
            case R.id.logout:
                showLogoutAlert();
                break;
        }

        drawer.closeDrawers();

        return true;
    }

    @Override
    public void fetchedHomeDashboardResponse(boolean status, String response) {
        loadDashboardFeature(response);
        getWallet2Balance();
    }

    /*
    private void initView() {
        amnt_txt = findViewById(R.id.amnt_txt);
        loan_id = findViewById(R.id.loan_id);
        aeps_submit_btn = findViewById(R.id.aeps_submit_btn);
        matm_submit_btn = findViewById(R.id.matm_submit_btn);
        rb_cw = findViewById(R.id.rb_cw);
        rb_be = findViewById(R.id.rb_be);
        rg_trans_type = findViewById(R.id.rg_trans_type);
        // micro_view = findViewById(R.id.micro_view);
        btnPair = findViewById(R.id.btnPair);
        upi_btn = findViewById(R.id.upi_btn);


        rg_trans_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_cw:
                        strTransType = "Cash Withdrawal";
                        amnt_txt.setText("");
                        amnt_txt.setEnabled(true);
                        amnt_txt.setVisibility(View.VISIBLE);
                        amnt_txt.setInputType(InputType.TYPE_CLASS_NUMBER);

                        break;
                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        amnt_txt.setText("");
                        amnt_txt.setVisibility(View.GONE);
                        amnt_txt.setClickable(false);
                        amnt_txt.setEnabled(false);
                        break;
                }
            }
        });

        matm_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // MATMSDKConstant.transactionAmount = amnt_txt.getText().toString().trim();
                if(rb_cw.isChecked()){
                    MATMSDKConstant.transactionType = MATMSDKConstant.cashWithdrawal;
                    MATMSDKConstant.transactionAmount = amnt_txt.getText().toString();
                    if(amnt_txt.getText().toString().trim().length()== 0){
                        Toast.makeText(MainActivity.this,"Please enter minimum Rs 100 ",Toast.LENGTH_SHORT).show();
                    }else if(Integer.parseInt(amnt_txt.getText().toString())<100){
                        Toast.makeText(MainActivity.this,"Please enter minimum Rs 100 ",Toast.LENGTH_SHORT).show();
                    }else{

                        if(PosActivity.isBlueToothConnected(MainActivity.this)){
                            MATMSDKConstant.paramA = "12345678";
                            MATMSDKConstant.paramB = "Branch1";
                            MATMSDKConstant.paramC = "loanID1234";
                            MATMSDKConstant.loginID = "aepsTestR";
                            MATMSDKConstant.encryptedData = "cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAPTvmZ48yn4JCqcQ5Z6H09U%2B8x0I80PAnJMXPAFqayZ2Ojh6mRYKWc7D2YuCQC9hAR";
                            Intent intent = new Intent(MainActivity.this, PosActivity.class);
                            startActivityForResult(intent, MATMSDKConstant.REQUEST_CODE);

                        }else {
                            Toast.makeText(MainActivity.this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show();
                        }
                    }


                }if(rb_be.isChecked()){
                    if(PosActivity.isBlueToothConnected(MainActivity.this)){
                        MATMSDKConstant.transactionType= MATMSDKConstant.balanceEnquiry;
                        MATMSDKConstant.transactionAmount = "0";
                        MATMSDKConstant.paramA = "12345678";
                        MATMSDKConstant.paramB = "Branch1";
                        MATMSDKConstant.paramC = "loanID1234";
                        MATMSDKConstant.loginID = "aepsTestR";
                        MATMSDKConstant.encryptedData = "cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAPTvmZ48yn4JCqcQ5Z6H09U%2B8x0I80PAnJMXPAFqayZ2Ojh6mRYKWc7D2YuCQC9hAR";
                        Intent intent = new Intent(MainActivity.this, PosActivity.class);
                        startActivityForResult(intent, MATMSDKConstant.REQUEST_CODE);
                    }else {
                        Toast.makeText(MainActivity.this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        aeps_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AepsSdkConstants.transactionAmount = amnt_txt.getText().toString().trim();
                if (rb_cw.isChecked()){
                    AepsSdkConstants.transactionType = AepsSdkConstants.cashWithdrawal;
                    AepsSdkConstants.transactionAmount = amnt_txt.getText().toString();


                    if(amnt_txt.getText().toString().trim().length()== 0){
                        Toast.makeText(MainActivity.this,"Please enter minimum Rs 1 ",Toast.LENGTH_SHORT).show();
                    }else if(Integer.parseInt(amnt_txt.getText().toString())<1){
                        Toast.makeText(MainActivity.this,"Please enter minimum Rs 1 ",Toast.LENGTH_SHORT).show();
                    }else{

                        AepsSdkConstants.paramA = "annapurna";
                        AepsSdkConstants.paramB = "BLS1";
                        AepsSdkConstants.paramC = "loanID";//loan_id.getText().toString().trim();
                        AepsSdkConstants.loginID = "aepsTestR";
                        AepsSdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqno3cxOh6wUl5r4YDSECZYVg%3D%3D";

                        Intent intent = new Intent(MainActivity.this, AEPSHomeActivity.class);
                        startActivityForResult(intent, AepsSdkConstants.REQUEST_CODE);
                    }

                }

                if(rb_be.isChecked()){
                    AepsSdkConstants.transactionType = AepsSdkConstants.balanceEnquiry;
                    AepsSdkConstants.transactionAmount = "1";

                    AepsSdkConstants.paramA = "annapurna";
                    AepsSdkConstants.paramB = "BLS1";
                    AepsSdkConstants.paramC = "loanID";//loan_id.getText().toString().trim();
                    AepsSdkConstants.loginID = "aepsTestR";
                    AepsSdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqno3cxOh6wUl5r4YDSECZYVg%3D%3D";

                    Intent intent = new Intent(MainActivity.this, AEPSHomeActivity.class);
                    startActivityForResult(intent, AepsSdkConstants.REQUEST_CODE);

                }

            }
        });

        btnPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                intent.putExtra("user_id","488");

                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                    Toast.makeText(getApplicationContext(),"Please Grant all the permissions", Toast.LENGTH_LONG).show();
                } else {
                    startActivity(intent);
                }
            }
        });

        upi_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this,UPItransactionActivity.class);
                //startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data !=null & resultCode==RESULT_OK){
            if(requestCode == AepsSdkConstants.REQUEST_CODE){
                String response = data.getStringExtra(AepsSdkConstants.responseData);
                System.out.println("Response:" +response);
            }else if(requestCode == MATMSDKConstant.REQUEST_CODE){
                String response = data.getStringExtra(MATMSDKConstant.responseData);
                System.out.println("Response:" +response);
            }
        }
    }*/

    private void loadProfileImage() {

        checkUserRole(_userName);
        //startTimer();
    }

    public void checkUserRole(final String admin_name) {
        try {

            AndroidNetworking.get(GET_USER_ROLE)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        getAdminDetails(encodedUrl, admin_name);
                                    }
                                });


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getAdminDetails(final String encoded_url, final String admin_name) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", admin_name);

            AndroidNetworking.post(encoded_url)
                    .addHeaders("Authorization", _token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (!isFinishing() && !isDestroyed()) {
                                    if (pd != null && pd.isShowing()) {
                                        pd.dismiss();
                                        pd.cancel();
                                    }
                                }

                                //  {"masterDistributor":"gsgswipew"}
                                JSONObject obj = new JSONObject(response.toString());

                                String status_master_distrubuter_value = obj.getString("masterDistributor");

                                if (status_master_distrubuter_value.trim().length() != 0) {
                                    image_url = "https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/MASTERDISTRIBUTOR_PROFILE%2F" + status_master_distrubuter_value + "%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";

                                } else {
                                    image_url = "https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F" + _adminName + "%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";
                                }

                                if (!isFinishing() && !isDestroyed()) {

                                    Picasso.with(MainActivity.this).load(image_url).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(customer_image);
                                    Picasso.with(MainActivity.this).load(image_url).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(customer2_image);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (!isFinishing() && !isDestroyed()) {
                                    if (pd != null && pd.isShowing()) {
                                        pd.dismiss();
                                        pd.cancel();
                                    }
                                }

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorBody();
                            if (!isFinishing() && !isDestroyed()) {
                                if (pd != null && pd.isShowing()) {
                                    pd.dismiss();
                                    pd.cancel();
                                }
                            }

                            image_url = "https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F" + _adminName + "%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";

                            if (!isFinishing() && !isDestroyed()) {
                                Picasso.with(MainActivity.this).load(image_url).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(customer_image);
                                Picasso.with(MainActivity.this).load(image_url).placeholder(R.drawable.ic_user).error(R.drawable.ic_user).into(customer2_image);
                                //                                Glide.with(getApplicationContext())
                                //                                        .load(image_url)
                                //                                        .apply(RequestOptions.placeholderOf(R.drawable.ic_user).error(R.drawable.ic_user))
                                //                                        .into(customer_image);
                                //
                                //                                Glide.with(getApplicationContext())
                                //                                        .load(image_url)
                                //                                        .apply(RequestOptions.placeholderOf(R.drawable.ic_user).error(R.drawable.ic_user))
                                //                                        .into(customer2_image);
                            }


                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


 /*   @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
        //startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }




    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(MainActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(MainActivity.this, user_name.toLowerCase(), status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/

    public void showLogoutAlert() {
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Do you want to logout from app";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        /*if(_userName!=null && session_logout==false) {
                            //checkSessionExistance(_userName,false);
                        }*/

                        SdkConstants.LogOut="0"; //For logout from SDK end
                        SdkConstants.BlueToothPairFlag="0";
                        Constants.bluetoothDevice = null;
                        sharePreferenceClass.clearData();


                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                    }
                })
                .show();
    }

    public void sessionAlert(Context context) {
        try {

            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Session Expired, Please login again.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        /*if(_userName!=null && session_logout==false) {
                            //checkSessionExistance(_userName,false);
                        }*/
                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
//                    .show();
        }catch (Exception e){

        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        showExitDialog();
    }

    private void showExitDialog() {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
            builder1.setMessage("Do you want to exit from this app");
            builder1.setTitle("Exit");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            SdkConstants.LogOut = "0"; //For logout from SDK end
                            SdkConstants.BlueToothPairFlag = "0";

                            dialog.cancel();
                            finish();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        catch (Exception e){

        }
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public void getWallet2Balance() {
        try {
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v72")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        getWallet2BalanceEncripted(encodedUrl);
                                    }
                                });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getWallet2BalanceEncripted(String encodedUrl) {

        // showLoader();

        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        // hideLoader();
                        reload2.setVisibility(View.VISIBLE);
                        progressBar2.setVisibility(View.GONE);
                        String wallet2_balance = response;
                        Double wallet2_bal = Double.valueOf(wallet2_balance);
                        das_user_balance2.setText("₹ " + wallet2_balance);
                        //getWallet1Balance();
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        // hideLoader();
                    }
                });
    }
}
