package com.isuisudmt.insurance;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.snackbar.Snackbar;
import com.isuisudmt.Constants;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameListActivity;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Insurance extends AppCompatActivity {
    TextView dob, state, age_validation, pincode_message, beneficiary_name, beneficiart_bank;
    EditText pincode, fname, lname, email_id, mobile, account_number;
    Calendar myCalendar;
    LinearLayout pincode_layout, customer_details, customer_bank_details;
    Button proceed, submit_capture_customer_details, submit;
    RadioGroup rg_valid_type;
    RadioButton rb_yes;
    RadioButton rb_no;
    CoordinatorLayout coordinatorLayout;
    SessionManager session;

    String _token = "", _admin = "", pennydropStatus = "";
    String district = "", subdistrict = "", village = "";
    String bank_code = "", bank_name = "";

    AutoCompleteTextView area;

    ArrayList<HashMap<String, String>> mList = new ArrayList<>();
    InsuranceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance2);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.insurance));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        Log.i("_token", "" + _token);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        dob = findViewById(R.id.dob);
        age_validation = findViewById(R.id.age_validation);

        pincode = findViewById(R.id.pincode_edit);
        pincode_message = findViewById(R.id.pincode_message);
        pincode_layout = findViewById(R.id.pincode_layout);
        customer_details = findViewById(R.id.customer_details);
        customer_bank_details = findViewById(R.id.customer_bank_details);
        proceed = findViewById(R.id.proceed);
        submit = findViewById(R.id.submit);
        submit_capture_customer_details = findViewById(R.id.submit_capture_customer_details);
        beneficiary_name = findViewById(R.id.beneficiary_name);
        beneficiart_bank = findViewById(R.id.beneficiart_bank);

        state = findViewById(R.id.state);
        area = findViewById(R.id.area);
        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        email_id = findViewById(R.id.email_id);
        mobile = findViewById(R.id.mobile);

        rg_valid_type = findViewById(R.id.rg_valid_type);
        rb_yes = findViewById(R.id.rb_yes);
        rb_no = findViewById(R.id.rb_no);
        account_number = findViewById(R.id.account_number);

        area.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 3) {
                    searchArea(s.toString());
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {

            }
        });

        area.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    district = mList.get(position).get("district");
                    subdistrict = mList.get(position).get("subdistrict");
                    village = mList.get(position).get("village");

                    area.setText(district + ", " + subdistrict + ", " + village);

                    Util.hideKeyboard(Insurance.this,area);

                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        });


        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.YEAR, year);

                        updateLabel(dayOfMonth, monthOfYear, year);
                    }
                };

                new DatePickerDialog(Insurance.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        account_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    if (count <= 2) {
                        beneficiary_name.setText("");
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {

                String mobile = s.toString();

                if (mobile.length() > 3) {
                    beneficiary_name.setText(fname.getText().toString() + " " + lname.getText().toString());
                }
            }
        });

        pincode.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    if (count != 6) {
                        pincode_layout.setVisibility(View.GONE);
                        state.setText("");
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("Mobile Number ::", "" + s.toString());
            }

            public void afterTextChanged(Editable s) {

                String mobile = s.toString();

                if (mobile.length() == 6) {

                    loadgeneratev55();
//                    pincode_layout.setVisibility(View.VISIBLE);
//                    pincode_message.setVisibility(View.VISIBLE);
//                    pincode.setVisibility(View.VISIBLE);
//
//                    CommonUtil.hideKeyboard(getApplicationContext(), pincode);
                }
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dob.getText().toString().isEmpty() || dob.getText().toString().equals("MM/DD/YYYY") || dob.getText().toString().trim().matches("")) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid Date of Birth.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (pincode.getText().toString().isEmpty() || pincode.getText().toString().trim().matches("")) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid pincode.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (area.getText().toString().isEmpty() || area.getText().toString().trim().matches("")) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter Area", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    proceed.setVisibility(View.GONE);
                    submit_capture_customer_details.setVisibility(View.VISIBLE);
                    customer_details.setVisibility(View.VISIBLE);
                    age_validation.setVisibility(View.VISIBLE);
                }
            }
        });

        submit_capture_customer_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pustInsuranceCodeWithoutBankInfo();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pushInsuranceCode();
            }
        });

        rg_valid_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_yes:

                        pennydropStatus = "true";

                        customer_bank_details.setVisibility(View.VISIBLE);
                        submit_capture_customer_details.setVisibility(View.GONE);

                        beneficiary_name.setText(fname.getText().toString() +", "+lname.getText().toString());

                        Util.hideKeyboard(Insurance.this, beneficiary_name);
                        break;
                    case R.id.rb_no:

                        pennydropStatus = "false";
                        submit_capture_customer_details.setVisibility(View.VISIBLE);
                        customer_bank_details.setVisibility(View.GONE);

                        bank_code = "";
                        bank_name = "";

                        beneficiart_bank.setText("");
                        account_number.setText("");
                        beneficiary_name.setText("");

                        Util.hideKeyboard(Insurance.this,beneficiary_name);
                        break;
                }
            }
        });

        beneficiart_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), BankNameListActivity.class);
                startActivityForResult(in, 100);
            }
        });
    }

    private void pustInsuranceCodeWithoutBankInfo() {

        if (dob.getText().toString().isEmpty() || dob.getText().toString().equals("MM/DD/YYYY") || dob.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid Date of Birth.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (pincode.getText().toString().isEmpty() || pincode.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid pincode.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (area.getText().toString().isEmpty() || area.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter Area", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (fname.getText().toString().isEmpty() || fname.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "First name required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (lname.getText().toString().isEmpty() || lname.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Last name required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (email_id.getText().toString().isEmpty() || email_id.getText().toString().trim().matches("") || !Util.isValidEmailId(email_id.getText().toString().trim())) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Email Id required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (mobile.getText() == null || mobile.getText().toString().trim().matches("") || !Util.isValidMobile(mobile.getText().toString().trim())) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Mobile number required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else {
            loadAPIv58();
        }
    }

    private void pushInsuranceCode() {

        if (dob.getText().toString().isEmpty() || dob.getText().toString().equals("MM/DD/YYYY") || dob.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid DOB.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (pincode.getText().toString().isEmpty() || pincode.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid Date of Birth.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (area.getText().toString().isEmpty() || area.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter Area", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (fname.getText().toString().isEmpty() || fname.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "First name required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (lname.getText().toString().isEmpty() || lname.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Last name required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (email_id.getText().toString().isEmpty() || email_id.getText().toString().trim().matches("") || !Util.isValidEmailId(email_id.getText().toString().trim())) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Email Id required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (mobile.getText() == null || mobile.getText().toString().trim().matches("") || !Util.isValidMobile(mobile.getText().toString().trim())) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Mobile number required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (bank_name.isEmpty() || bank_name.trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Select Bank", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (account_number.getText().toString().isEmpty() || account_number.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Account number required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if (beneficiary_name.getText().toString().isEmpty() || beneficiary_name.getText().toString().trim().matches("")) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Beneficiary name required.", Snackbar.LENGTH_LONG);
            snackbar.show();
        } else {
            loadAPIv58();
        }
    }

    private void searchArea(final String _area) {

        Log.i("area >>>>>>>> ", "" + _area);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("state", _area);
        params.put("city", state.getText().toString());

        RequestQueue mQueue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "https://itpl.iserveu.tech/generate/v57", new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("response :: ", "" + response);
                            String key = response.getString("hello");
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            Log.i("encodedUrl :: ", "" + encodedUrl);

                            searchResult(encodedUrl);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        mQueue.add(jsonObjectRequest);

    }

    private void searchResult(String encodedUrl) {
        Log.i("api url ::::::: ", "" + encodedUrl);

        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Log.i("search area >>>>", "" + response);

                        if (response.optInt("status") == 0) {

                            mList.clear();

                            try {
                                JSONArray jsonArray = response.optJSONArray("address");

                                for (int j = 0; j < jsonArray.length(); j++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(j);

                                    String district = jsonObject.optString("district").trim();
                                    String subdistrict = jsonObject.optString("subdistrict").trim();
                                    String village = jsonObject.optString("village").trim();

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("district", district);
                                    map.put("subdistrict", subdistrict);
                                    map.put("village", village);

                                    mList.add(map);

                                }

                                adapter = new InsuranceAdapter(getApplicationContext(), R.layout.rowlayout, R.id.beneMobileNo, mList);
                                area.setThreshold(1);
                                area.setAdapter(adapter);
                                area.setTextColor(Color.BLACK);

                                adapter.setNotifyOnChange(true);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.i("search issue >>>>", "" + error.getResponse());
                    }
                });

    }

    private void loadgeneratev55() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("number", pincode.getText().toString());

        RequestQueue mQueue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("https://itpl.iserveu.tech/generate/v55", new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("response :: ", "" + response);
                            String key = response.getString("hello");
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            Log.i("encodedUrl :: ", "" + encodedUrl);

                            loadPincode(encodedUrl);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        mQueue.add(jsonObjectRequest);
    }

    private void loadAPIv58() {
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v58")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Log.i("obj >>>>", "" + response);

                        try {
                            Log.i("response :: ", "" + response);
                            String key = response.getString("hello");
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            Log.i("encodedUrl :: ", "" + encodedUrl);

                            submitFinalCode(encodedUrl);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.i("encodedUrl >>>>", "" + error.getResponse());
                    }
                });
    }

    private void loadPincode(String encodedUrl) {

        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject obj) {
                        // do anything with response
                        Log.i("obj >>>>", "" + obj);

                        try {
                            int status = obj.getInt("status");
                            String statusDesc = obj.getString("statusDesc");

                            if (status == 0) {

                                Util.hideKeyboard(Insurance.this,beneficiary_name);

                                String _state = obj.getJSONObject("address").getString("state");
                                String _pincode = obj.getJSONObject("address").getString("pincode");

                                Log.i("state >>>>", "" + _state);
                                Log.i("pincode >>>>", "" + _pincode);

                                state.setText(_state);

                                pincode_layout.setVisibility(View.VISIBLE);

                            } else {
                                state.setText("");

                                Toast.makeText(Insurance.this, "" + statusDesc, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.i("encodedUrl >>>>", "" + error.getResponse());
                    }
                });
    }

    private void submitFinalCode(String encodedUrl) {
        String _dob = dob.getText().toString();
        String _pincode = pincode.getText().toString();
        String _state = state.getText().toString();
        String _fname = fname.getText().toString();
        String _lname = lname.getText().toString();
        String _email = email_id.getText().toString();
        String _mobileno = mobile.getText().toString();
        String _accountno = account_number.getText().toString();
        String _beneficiary_name = beneficiary_name.getText().toString();

        Log.i("_dob", _dob);
        Log.i("_pincode", _pincode);
        Log.i("_state", _state);
        Log.i("_fname", _fname);
        Log.i("_lname", _lname);
        Log.i("_email", _email);
        Log.i("_mobileno", _mobileno);
        Log.i("_accountno", _accountno);
        Log.i("_beneficiary_name", _beneficiary_name);
        Log.i("district", district);
        Log.i("subDistrict", subdistrict);
        Log.i("village", village);
        Log.i("accountHolderName", _beneficiary_name);
        Log.i("bank_name", bank_name);
        Log.i("_accountno", _accountno);


        JSONObject params = new JSONObject();

        try {
            params.put("firstName", _fname);
            params.put("lastName", _lname);
            params.put("email", _email);
            params.put("mobilePhone", _mobileno);
            params.put("dateOfBirth", _dob);
            params.put("pincode", _pincode);
            params.put("state", _state);
            params.put("district", district);
            params.put("subDistrict", subdistrict);
            params.put("village", village);
            params.put("pennydropStatus", pennydropStatus);
            params.put("accountHolderName", _beneficiary_name);
            params.put("bankName", bank_name);
            params.put("accountNumber", _accountno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Make request for JSONObject
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, encodedUrl, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.optInt("status") == 0) {
                                finish();
                                startActivity(new Intent(getApplicationContext(), InsuranceWabviewActivity.class).putExtra("url", response.getString("redirectLink")));
                            } else {
                                Toast.makeText(Insurance.this, "Try again!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("xzczc", "Error: " + error.getMessage());
            }
        }) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", _token);
                return headers;
            }
        };

        // Adding request to request queue
        Volley.newRequestQueue(this).add(jsonObjReq);

    }

    private void updateLabel(int dayOfMonth, int monthOfYear, int year) {
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        try {
            int age = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));

            if (age >= 18 && age <= 55) {
                dob.setText(sdf.format(myCalendar.getTime()));
                pincode_message.setVisibility(View.VISIBLE);
                pincode.setVisibility(View.VISIBLE);

                age_validation.setVisibility(View.VISIBLE);

                pincode.setFocusable(true);
                pincode.setCursorVisible(true);
            } else {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.valid_age), Snackbar.LENGTH_LONG);
                snackbar.show();

                /*age_validation.setVisibility(View.VISIBLE);
                pincode_message.setVisibility(View.GONE);
                pincode.setVisibility(View.GONE);
                pincode_layout.setVisibility(View.GONE);
                customer_details.setVisibility(View.GONE);
                customer_bank_details.setVisibility(View.GONE);*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 100) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                beneficiart_bank.setText(bankIINValue.getBankName());

                bank_code = bankIINValue.getIin();
                bank_name = bankIINValue.getBankName();
            }
        }
    }

}
