package com.isuisudmt.mpin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.SplashActivity;
import com.isuisudmt.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthMPINActivity extends AppCompatActivity {
    private static final String TAG = AuthMPINActivity.class.getSimpleName();

    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref" ;
    public static final String USER_NAME = "userNameKey";
    public static final String USER_MPIN = "mpinKey";

    String token;

    String userName, mpin;

    ProgressDialog dialog;

    TextView generate;
    Button verify;
    TextInputLayout mpinLayout;
    TextInputEditText mpinET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_mpin);

        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);

        userName = sp.getString(USER_NAME, "");
        mpin = sp.getString(USER_MPIN, "");
        token = getIntent().getStringExtra("token");
        Toolbar toolbar = findViewById(R.id.mpin_toolbar);
        toolbar.setTitle("Enter MPIN");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);

        generate = findViewById(R.id.mpin_generate);
        verify = findViewById(R.id.mpin_verify);
        mpinLayout = findViewById(R.id.mpin_layout);
        mpinET = findViewById(R.id.mpin);

        mpinET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mpinLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = mpinET.getText().toString().trim();
                if (s.length() == 0){
                    mpinLayout.setError("Enter pin");
                } else {
                    loginWithMpin(userName, s);
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(AuthMPINActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

//        if (userName == null || userName.length() == 0){
//            // add username to shared preference and ask to enter mpin and validate
//
//            // add username to sp
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString(USER_NAME, "itpl");
//            editor.apply();
//
//            //validate
//        }
//        else {
//            if (mpin == null || mpin.length() == 0){
//                // ask to enter mpin and validate
//            } else {
//                // do auto check and login
//                loginWithMpin(userName, mpin, "auto");
//            }
//        }

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateMpin(userName);
            }
        });

    }

    private void generateMpin(String un){
        dialog = new ProgressDialog(AuthMPINActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/user_mpin/generate_mpin";
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", token);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");
                                if (status == 1) {
                                    Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                                }
                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e(TAG, "onError: ANError " + anError.getErrorBody());
                            try {
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());
                                String message = errorObject.getString("message");
                                Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void loginWithMpin(String uN, String pin) {

        dialog = new ProgressDialog(AuthMPINActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/user_mpin/login";
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", token);
            obj.put("m_pin", pin);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");

                                if (status == 1){
                                    //load main activity
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(USER_MPIN, pin);
                                    editor.apply();

                                    Intent i = new Intent(AuthMPINActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());
                                int status = errorObject.getInt("status");
                                if (status == 0){
                                    String message = "MPIN has been changed.\nEnter the correct one or regenerate.";
                                    Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                                } else if (status == -1){
                                    String message = "MPIN has been expired.\nRegenrated the MPIN.";
                                    Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                                }

                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(USER_MPIN, "");
                                editor.apply();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}