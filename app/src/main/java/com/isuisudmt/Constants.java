package com.isuisudmt;


import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;


public class Constants {


    public static final String BASE_URL = "https://itpl.iserveu.tech/";
    //    public static final String BASE_URL = "http://35.200.175.157:8080";
//    public static final String BASE_URL = "http://35.220.171.29:8080";
    //public static final String recharge = "https://iserveu.xyz/";
    public static final String recharge = "https://itpl.iserveu.tech/";

    public static final String BASE_URL_FUND_TRANSFER = "https://habizglobal.in/";
    public static final int REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE = 1003;
    public static final int REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE = 1001;
    public static final int REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE = 1002;
    public static final int BALANCE_RELOAD = 1004;
    public static final String IIN_KEY = "IIN_KEY";
    public static final String USB_DEVICE = "USB_DEVICE";
    public static final String TRANSACTION_STATUS_KEY = "TRANSACTION_STATUS_KEY";
    public static final String MICRO_ATM_TRANSACTION_STATUS_KEY = "MICRO_ATM_TRANSACTION_STATUS_KEY";
    public static final String AEPS_2_TRANSACTION_STATUS_KEY = "AEPS_2_TRANSACTION_STATUS_KEY";
    public static final String TRANSACTION_STATUS_KEY_SDK = "TRANSACTION_STATUS_KEY_SDK";
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String NEXT_FRESHNESS_FACTOR = "NEXT_FRESHNESS_FACTOR";
    public static final String EASY_AEPS_PREF_KEY = "EASY_AEPS_PREF_KEY";
    public static final String EASY_AEPS_USER_LOGGED_IN_KEY = "EASY_AEPS_USER_LOGGED_IN_KEY";
    public static final String EASY_AEPS_USER_NAME_KEY = "EASY_AEPS_USER_NAME_KEY";
    public static final String EASY_AEPS_PASSWORD_KEY = "EASY_AEPS_PASSWORD_KEY";
    public static final String TEST_USER_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTI5OTkyMDQxNTE1LCJleHAiOjE1MzAwMjgwNDF9.Se0zZhaoemx6eeAVlE_9N_LqkhylDgsJS8f1-5Y4uIYIfFV_7Gst5JhwnA-ogjs4whk9x9VnNvBL8T-AVkctnQ";
    public static final String RETURN_URL = "http://10.15.20.131:8023/PG/TestPage.aspx";
    public static final String VERSION = "1000";
    public static final String SERVICE_AEPS_TS = "AEPStransactionStatus";
    public static final String SERVICE_AEPS_CW = "AEPScashWithdrawal";
    public static final String SERVICE_AEPS_BE = "AEPSbalanceEnquiry";
    public static final String SERVICE_MICRO_TS = "MATMtransactionStatus";
    public static final String SERVICE_MICRO_CW = "MATMcashWithdrawal";
    public static final String SERVICE_MICRO_BE = "MATMbalanceEnquiry";
    public static final String CLIENT_REQUEST_ENCRYPTION_KEY = "11e9903b-7cde-4738-96fe-c8295c8232de";
    //    public static final String CLIENT_REQUEST_ENCRYPTION_KEY = "5c542938-904a-49fa-b981-10203c7db1f1";
    public static final String CLIENT_HEADER_ENCRYPTION_KEY = "982b0d01-b262-4ece-a2a2-45be82212ba1";
    public static final String MERCHANT_ID = "8270629964";
    public static final String AUTHKEY = "333-444-555";
    public static final String CLIENTID = "8";
    public static final int RELOADREPORTS = 1005;
    public static String USER_NAME = "";
    public static String ADMIN_NAME = "";

    public static String USER_SHOP_NAME="";
    public static String USER_MOBILE_NO ="";

    public static String USER_VERIFICATION_STATUS="unverified";

    public static String VERIFIED_USERID="";
    public static String VERIFIED_USERPHONE="";
    public static String VERIFIED_USER_MPIN="";
    public static String VERIFIED_USER_PWD="";
    public static String VERIFIED_USER_EMAIL="";
    public static String VERIFIED_USER_VIDEO="";
    public static String VERIFIED_USER_LOC="";
    public static String VERIFIED_USER_LACT="";
    public static String VERIFIED_USER_LONG="";

    public static boolean USER_WALLET_2_ACTIVE_STATUS = false;
    public static boolean USER_WALLET_3_ACTIVE_STATUS = false;
    public static int session_time = 300000;
    public static BluetoothDevice bluetoothDevice= null;
    public static String SHOP_NAME="";
    public static String BRAND_NAME="";
    public static String BANK_FLAG = "";
    public static ArrayList<String> user_feature_array= new ArrayList<>();


    public static String bank_name_list = "[\n" +
            "    {\"BANKNAME\":\"Allahabad Bank\",\"IIN\":\"608112\"},\n" +
            "    {\"BANKNAME\":\"Allahabad UP Gramin Bank\",\"IIN\":\"607091\"},\n" +
            "    {\"BANKNAME\":\"Andhra Bank\",\"IIN\":\"607076\"},\n" +
            "    {\"BANKNAME\":\"Andhra Pradesh Gramin Vikash Bank\",\"IIN\":\"607198\"},\n" +
            "    {\"BANKNAME\":\"Andhra Pragathi Grameena Bank\",\"IIN\":\"607121\"},\n" +
            "    {\"BANKNAME\":\"Assam Gramin Vikash Bank\",\"IIN\":\"607064\"},\n" +
            "    {\"BANKNAME\":\"Axis Bank\",\"IIN\":\"607153\"},\n" +
            "    {\"BANKNAME\":\"Bangiya Gramin Vikash Bank\",\"IIN\":\"607063\"},\n" +
            "    {\"BANKNAME\":\"Bank Of Baroda\",\"IIN\":\"606985\"},\n" +
            "    {\"BANKNAME\":\"Bank of India\",\"IIN\":\"508505\"},\n" +
            "    {\"BANKNAME\":\"Bank of Maharashtra\",\"IIN\":\"607387\"},\n" +
            "    {\"BANKNAME\":\"Baroda Gujarat Gramin Bank \",\"IIN\":\"606995\"},\n" +
            "    {\"BANKNAME\":\"Baroda Rajasthan Kshetriya Gramin Bank\",\"IIN\":\"607280\"},\n" +
            "    {\"BANKNAME\":\"Baroda Uttar Pradesh Gramin Bank \",\"IIN\":\"606993\"},\n" +
            "    {\"BANKNAME\":\"Bihar Gramin Bank\",\"IIN\":\"607377\"},\n" +
            "    {\"BANKNAME\":\"Canara Bank\",\"IIN\":\"607396\"},\n" +
            "    {\"BANKNAME\":\"Central Bank of India\",\"IIN\":\"607264\"},\n" +
            "    {\"BANKNAME\":\"Central Madya Pradesh Gramin Bank\",\"IIN\":\"607071\"},\n" +
            "    {\"BANKNAME\":\"City Union Bank \",\"IIN\":\"607324\"},\n" +
            "    {\"BANKNAME\":\"Chaitanya Godavari Gramin Bank\",\"IIN\":\"607080\"},\n" +
            "    {\"BANKNAME\":\"Chhattisgharh Rajya Gramin Bank\",\"IIN\":\"607214\"},\n" +
            "    {\"BANKNAME\":\"Corporation Bank\",\"IIN\":\"607184\"},\n" +
            "    {\"BANKNAME\":\"Dena Bank\",\"IIN\":\"508547\"},\n" +
            "    {\"BANKNAME\":\"Dena Gujarat Gramin Bank\",\"IIN\":\"607099\"},\n" +
            "    {\"BANKNAME\":\"Ellaquai Dehati Bank\",\"IIN\":\"607218\"},\n" +
            "    {\"BANKNAME\":\"Gramin bank of Aryavart\",\"IIN\":\"607024\"},\n" +
            "    {\"BANKNAME\":\"HDFC Bank\",\"IIN\":\"607152\"},\n" +
            "    {\"BANKNAME\":\"Himachal Pradesh Gramin Bank\",\"IIN\":\"607140\"},\n" +
            "    {\"BANKNAME\":\"ICICI Bank\",\"IIN\":\"508534\"},\n" +
            "    {\"BANKNAME\":\"IDBI Bank\",\"IIN\":\"607095\"},\n" +
            "    {\"BANKNAME\":\"IDFC Bank\",\"IIN\":\"608117\"},\n" +
            "    {\"BANKNAME\":\"Indian bank\",\"IIN\":\"607105\"},\n" +
            "    {\"BANKNAME\":\"Indian Overseas Bank\",\"IIN\":\"607126\"},\n" +
            "    {\"BANKNAME\":\"IndusInd Bank\",\"IIN\":\"607189\"},\n" +
            "    {\"BANKNAME\":\"Jammu & Kashmir Bank \",\"IIN\":\"607440\"},\n" +
            "    {\"BANKNAME\":\"Jharkhand Gramin Bank\",\"IIN\":\"607021\"},\n" +
            "    {\"BANKNAME\":\"Karnataka Bank Ltd\",\"IIN\":\"607270\"},\n" +
            "    {\"BANKNAME\":\"Kashi Gomati Samyut Gramin Bank\",\"IIN\":\"607365\"},\n" +
            "    {\"BANKNAME\":\"Karnataka Vikas Grameena Bank \",\"IIN\":\"607122\"},\n" +
            "    {\"BANKNAME\":\"Karur Vysya Bank\",\"IIN\":\"508662\"},\n" +
            "    {\"BANKNAME\":\"Kaveri Grameena Bank\",\"IIN\":\"607308\"},\n" +
            "    {\"BANKNAME\":\"Kerala Gramin Bank\",\"IIN\":\"607399\"},\n" +
            "    {\"BANKNAME\":\"Langpi Dehangi Rural Bank\",\"IIN\":\"607202\"},\n" +
            "    {\"BANKNAME\":\"Madhya Bihar Gramin Bank\",\"IIN\":\"607136\"},\n" +
            "    {\"BANKNAME\":\"Madhyanchal Gramin Bank\",\"IIN\":\"607232\"},\n" +
            "    {\"BANKNAME\":\"Malwa Gramin Bank\",\"IIN\":\"607241\"},\n" +
            "    {\"BANKNAME\":\"Manipur Rural Bank \",\"IIN\":\"607062\"},\n" +
            "    {\"BANKNAME\":\"Meghalaya Rural Bank\",\"IIN\":\"607206\"},\n" +
            "    {\"BANKNAME\":\"Narmada Jhabua Gramin Bank\",\"IIN\":\"607022\"},\n" +
            "    {\"BANKNAME\":\"Odisha Gramya Bank \",\"IIN\":\"607060\"},\n" +
            "    {\"BANKNAME\":\"Oriental Bank of Commerce\",\"IIN\":\"607085\"},\n" +
            "    {\"BANKNAME\":\"Pallavan Grama Bank\",\"IIN\":\"607052\"},\n" +
            "    {\"BANKNAME\":\"Pandyan GB\",\"IIN\":\"607059\"},\n" +
            "    {\"BANKNAME\":\"Paschim Banga Gramin Bank\",\"IIN\":\"607079\"},\n" +
            "    {\"BANKNAME\":\"Pragathi Krishna Gramin Bank\",\"IIN\":\"607400\"},\n" +
            "    {\"BANKNAME\":\"Prathama Bank\",\"IIN\":\"607124\"},\n" +
            "    {\"BANKNAME\":\"Puduvai Bharthiar Grama Bank\",\"IIN\":\"607054\"},\n" +
            "    {\"BANKNAME\":\"Punjab & Sind Bank\",\"IIN\":\"607087\"},\n" +
            "    {\"BANKNAME\":\"Punjab Gramin Bank\",\"IIN\":\"607138\"},\n" +
            "    {\"BANKNAME\":\"Punjab National Bank\",\"IIN\":\"607027\"},\n" +
            "    {\"BANKNAME\":\"Purvanchal Gramin Bank\",\"IIN\":\"607212\"},\n" +
            "    {\"BANKNAME\":\"Rajasthan Marudhara Gramin Bank\",\"IIN\":\"607509\"},\n" +
            "    {\"BANKNAME\":\"Ratnakar Bank\",\"IIN\":\"607393\"},\n" +
            "    {\"BANKNAME\":\"Saptagiri Grameena Bank\",\"IIN\":\"607053\"},\n" +
            "    {\"BANKNAME\":\"Sarva Haryana Gramin Bank\",\"IIN\":\"607139\"},\n" +
            "    {\"BANKNAME\":\"Sarva UP Gramin Bank \",\"IIN\":\"607135\"},\n" +
            "    {\"BANKNAME\":\"Saurashtra Gramin Bank\",\"IIN\":\"607200\"},\n" +
            "    {\"BANKNAME\":\"Shivalik Mercantile Cooperative Bank \",\"IIN\":\"607119\"},\n" +
            "    {\"BANKNAME\":\"South Indian Bank\",\"IIN\":\"607475\"},\n" +
            "    {\"BANKNAME\":\"State Bank of India\",\"IIN\":\"607094\"},\n" +
            "    {\"BANKNAME\":\"Sutlej Gramin Bank\",\"IIN\":\"607310\"},\n" +
            "    {\"BANKNAME\":\"Syndicate Bank\",\"IIN\":\"607580\"},\n" +
            "    {\"BANKNAME\":\"Tamilnad Mercantile Bank\",\"IIN\":\"607187\"},\n" +
            "    {\"BANKNAME\":\"Telangana Grameena Bank\",\"IIN\":\"607195\"},\n" +
            "    {\"BANKNAME\":\"The Saraswat Co-operative Bank Ltd\",\"IIN\":\"652150\"},\n" +
            "    {\"BANKNAME\":\"Tripura Gramin Bank\",\"IIN\":\"607065\"},\n" +
            "    {\"BANKNAME\":\"UCO Bank\",\"IIN\":\"607066\"},\n" +
            "    {\"BANKNAME\":\"Union Bank of India\",\"IIN\":\"607161\"},\n" +
            "    {\"BANKNAME\":\"United Bank Of India\",\"IIN\":\"607646\"},\n" +
            "    {\"BANKNAME\":\"Utkal Gramin Bank\",\"IIN\":\"607234\"},\n" +
            "    {\"BANKNAME\":\"Uttar Banga Kshetriya Gramin Bank \",\"IIN\":\"607073\"},\n" +
            "    {\"BANKNAME\":\"Uttar Bihar Grameen Bank \",\"IIN\":\"607069\"},\n" +
            "    {\"BANKNAME\":\"Uttarakhand Gramin Bank\",\"IIN\":\"607197\"},\n" +
            "    {\"BANKNAME\":\"Vananchal Gramin Bank\",\"IIN\":\"607210\"},\n" +
            "    {\"BANKNAME\":\"Vidarbha Konkan Gramin Bank\",\"IIN\":\"607020\"},\n" +
            "    {\"BANKNAME\":\"Vijaya Bank\",\"IIN\":\"607075\"}]";

}
