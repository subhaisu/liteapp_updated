package com.isuisudmt.changePassword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.forgotpassword.ForgotPasswordActivity;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.utility.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangePasswordActivity extends AppCompatActivity {


    TextInputEditText confirm_pwd, otp, new_pwd, old_pwd;
    TextInputLayout otpLayout, newPwdLayout, oldPwdLayout, confirmPwdLayout;
    Button sendOtp, changePwdBtn;
    String newPassword, oldPassword;
    int userOtp = 0;
    ProgressBar progressBar;
    TextView resendotp;
    String tokenStr = "";
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        session = new SessionManager(ChangePasswordActivity.this);
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        confirm_pwd = findViewById(R.id.confirm_pwd);
        otp = findViewById(R.id.otp);
        new_pwd = findViewById(R.id.new_pwd);
        old_pwd = findViewById(R.id.old_pwd);
        otpLayout = findViewById(R.id.otpLayout);
        newPwdLayout = findViewById(R.id.newPwdLayout);
        oldPwdLayout = findViewById(R.id.oldPwdLayout);
        confirmPwdLayout = findViewById(R.id.confirmPwdLayout);
        sendOtp = findViewById(R.id.sendOtp);
        changePwdBtn = findViewById(R.id.changePwdBtn);
        progressBar = findViewById(R.id.progressBar);
        resendotp = findViewById(R.id.resendotp);


        old_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    old_pwd.setError("Enter the exact old Password");
                }
            }
        });

        new_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    new_pwd.setError("Enter the maximum 6 characters");
                }

                    if (!isValidPassword(new_pwd.getText().toString())) {
                        new_pwd.setError("Password must contain mix of upper and lower case letters as well as digits and one special charecter(4-20)");
                    }


            }
        });

        confirm_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    confirm_pwd.setError("Enter the maximum 6 characters");
                }

                    if (!isValidPassword(confirm_pwd.getText().toString()) ) {
                        confirm_pwd.setError("Password must contain mix of upper and lower case letters as well as digits and one special charecter(4-20)");
                    }

                //String con_pwd = confirm_pwd.getText().toString().trim();
              //  String n_pwd = new_pwd.getText().toString().trim();
                /*if(con_pwd.equals(n_pwd)){
                }else{
                    confirm_pwd.setError("Confirm Password should match with New Password");

                }*/

            }
        });

        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPassword = old_pwd.getText().toString().trim();
                newPassword = new_pwd.getText().toString().trim();
                String con_pwd = confirm_pwd.getText().toString().trim();




                if (oldPassword.length() != 0 && newPassword.length() != 0) {
                    /*Call api to send the otp to the registered mobile number*/

                    if(!oldPassword.equalsIgnoreCase(newPassword)){

                        if(newPassword.equalsIgnoreCase(con_pwd)){
                            ConnectionDetector cd = new ConnectionDetector(
                                    ChangePasswordActivity.this);
                            if (cd.isConnectingToInternet()) {

                                progressBar.setVisibility(View.VISIBLE);
                                getEncodeUrlCode();


                            } else {
                                Toast.makeText(ChangePasswordActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(ChangePasswordActivity.this, "Confirm password should match with new password", Toast.LENGTH_LONG).show();

                        }

                    }else{
                        Toast.makeText(ChangePasswordActivity.this, "Old and new password can't be same .", Toast.LENGTH_LONG).show();

                    }
                }

            }
        });

        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /*Call api to send the otp to the registered mobile number*/
                ConnectionDetector cd = new ConnectionDetector(
                        ChangePasswordActivity.this);
                if (cd.isConnectingToInternet()) {

                    if (oldPassword.length() != 0 && newPassword.length() != 0) {
                        progressBar.setVisibility(View.VISIBLE);
                        getEncodeUrlCode();


                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        changePwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                oldPassword = old_pwd.getText().toString().trim();
                newPassword = new_pwd.getText().toString().trim();
                String otpvalue = otp.getText().toString().trim();
                userOtp = Integer.parseInt(otpvalue);

                /*Call api to send the otp to the registered mobile number*/
                ConnectionDetector cd = new ConnectionDetector(
                        ChangePasswordActivity.this);
                if (cd.isConnectingToInternet()) {

                    if (oldPassword.length() != 0 && newPassword.length() != 0 && userOtp != 0) {
                        progressBar.setVisibility(View.VISIBLE);
                        getEncodeUrlCodeforPwd();


                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }


    private void getEncodeUrlCode() {

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v97")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            sendOtp(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                    }

                });
    }

    private void sendOtp(String base64) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("oldPassword", oldPassword);
            obj.put("newPassword", newPassword);
            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                if (status.equals("0")) {
                                    changePwdBtn.setVisibility(View.VISIBLE);
                                    otpLayout.setVisibility(View.VISIBLE);
                                    oldPwdLayout.setVisibility(View.GONE);
                                    newPwdLayout.setVisibility(View.GONE);
                                    confirmPwdLayout.setVisibility(View.GONE);
                                    resendotp.setVisibility(View.VISIBLE);
                                    sendOtp.setVisibility(View.GONE);
                                    Toast.makeText(ChangePasswordActivity.this, obj.getString("statusDesc").toString(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ChangePasswordActivity.this, "", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.GONE);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Toast.makeText(ChangePasswordActivity.this, obj.getString("statusDesc").toString(), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getEncodeUrlCodeforPwd() {

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v96")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            ChangePassword(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                    }

                });
    }

    private void ChangePassword(String base64) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("oldPassword", oldPassword);
            obj.put("password", newPassword);
            obj.put("otp", userOtp);

            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                String msg = obj.getString("statusDesc");
                                if (status.equals("0")) {
                                    Toast.makeText(ChangePasswordActivity.this, msg, Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(ChangePasswordActivity.this, msg, Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.GONE);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Toast.makeText(ChangePasswordActivity.this, obj.getString("statusDesc").toString(), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isValidPassword(String password) {
        Matcher matcher = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{4,20})").matcher(password);
        return matcher.matches();
    }
}