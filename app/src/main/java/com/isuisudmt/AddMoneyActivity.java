package com.isuisudmt;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.dmt.Currency;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameListActivity;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import static com.isuisudmt.Util.hasPermissions;

public class AddMoneyActivity extends AppCompatActivity implements PermissionManager.OnRequestPermissionListener {
    private EditText cash_withdrawal_bankspinner,amountTxt;
    private TextView amoutplainTxt,dtTxt;
    Calendar myCalendar;
    private ImageView add_photo;
    private ProgressBar progressLoad;

    private static final int REQUEST_SELECT_PICTURE = 222;
    public static int CAMERA_CODE = 64;
    public static Uri mImageCaptureUri;

    PermissionManager manager;
    SessionManager sessionManager;
    Button requestBtn;
    ProgressBar progressV;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);

        manager = new PermissionManager(AddMoneyActivity.this);
        sessionManager = new SessionManager(AddMoneyActivity.this);

        HashMap<String, String> user = sessionManager.getUserDetails();
        HashMap<String, String> userDetails = sessionManager.getUserSession();

        token = user.get(SessionManager.KEY_TOKEN);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.mywallet));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        progressV = findViewById(R.id.progressV);
        requestBtn = findViewById(R.id.requestBtn);
        amountTxt = findViewById(R.id.amountTxt);
        amoutplainTxt = findViewById(R.id.amoutplainTxt);
        dtTxt = findViewById(R.id.dtTxt);
        progressLoad = findViewById(R.id.progressLoad);
        add_photo = findViewById(R.id.add_photo);
        add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // progressLoad.setVisibility(View.VISIBLE);
                String[] needed_permissions=new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if(hasPermissions(AddMoneyActivity.this,needed_permissions)){
                    OpenCameraDialog();
                }else{
                    //OpenCameraDialog();
                    manager.checkAndRequestPermissions(AddMoneyActivity.this,needed_permissions);
                }
            }
        });

        dtTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.YEAR, year);

                        updateLabel(dayOfMonth, monthOfYear, year);
                    }
                };

                new DatePickerDialog(AddMoneyActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });



        amountTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString().isEmpty()){
                    amoutplainTxt.setText("");

                }else {

                    String str = Currency.convertToIndianCurrency(s.toString());
                    amoutplainTxt.setText(str);
                }

            }
        });

        cash_withdrawal_bankspinner = findViewById(R.id.cash_withdrawal_bankspinner);
        cash_withdrawal_bankspinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showLoader();
                Intent in = new Intent(AddMoneyActivity.this, BankNameListActivity.class);
                startActivityForResult(in, Constants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE);
            }
        });
        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callRequestData();
            }
        });
    }

    private void callRequestData() {
        progressV.setVisibility(View.VISIBLE);
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v59")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            System.out.println(">>>>-----"+encodedUrl);
                            callEncriptedRequestData(encodedUrl,progressV);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        progressV.setVisibility(View.GONE);
                    }
                });
    }

    private void callEncriptedRequestData(String encodedUrl, final ProgressBar progressV) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("senderName", "");
            obj.put("senderBankName", "ANDHRA BANK");
            obj.put("senderAccountNo","");
            obj.put("depositDate", "2020-02-25");
            obj.put("amount", "1");
            obj.put("transferType", "IMPS");
            obj.put("bankRefId", "erw21");
            obj.put("remarks", "jj");
            obj.put("receiptDownloadUrl","https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/Balance%20Request%2Fdemoisu%2Fitpl%2FreceiptTue%20Feb%2025%202020%2019%3A45%3A13%20GMT%2B0530%20(India%20Standard%20Time).png?alt=media&token=447aa89f-9c8f-4ace-9d23-36ee8887ce58");


            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("authorization",token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressV.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("status");
                                if (statusCode.equalsIgnoreCase("0")) {
                                    String statusDescription = obj.getString("statusDesc");
                                    Toast.makeText(AddMoneyActivity.this, "Success", Toast.LENGTH_SHORT).show();




                                } else {
                                    Toast.makeText(AddMoneyActivity.this, "Failure", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(AddMoneyActivity.this, anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                            progressV.setVisibility(View.GONE);
                        }
                    });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void OpenCameraDialog() {
        final Dialog dialog = new Dialog(AddMoneyActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.open_camera_dialogrecharge);

       Button btnGallery = dialog.findViewById(R.id.btnGallery);
       Button btnCamera = dialog.findViewById(R.id.btnCamera);
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickImage(dialog);
            }
        });
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CaptureImage(dialog);
            }
        });



        dialog.show();


    }

/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
            cash_withdrawal_bankspinner.setText(bankIINValue.getBankName());
           // balance_enquiry_nnid = bankIINValue.getIin();
            //cash_withdrawal_nnid = "";
            // checkBalanceEnquiryValidation();


        }
    }*/

    private void PickImage(final Dialog dl) {
        File folder = new File(Environment.getExternalStorageDirectory().toString() + "/Coreapp");
        folder.mkdirs();
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_SELECT_PICTURE);
    }

    public void CaptureImage(final Dialog dl) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            File folder = new File(Environment.getExternalStorageDirectory().toString() + "/Coreapp");
            folder.mkdirs();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(Environment.getExternalStorageDirectory() + "/Coreapp", "image.jpg");
            mImageCaptureUri = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(intent, CAMERA_CODE);

        } else {
            File folder = new File(Environment.getExternalStorageDirectory().toString() + "/Coreapp");
            folder.mkdirs();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(Environment.getExternalStorageDirectory() + "/Coreapp", "image.jpg");
            mImageCaptureUri = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(intent, CAMERA_CODE);
        }


    }

    private void updateLabel(int dayOfMonth, int monthOfYear, int year) {
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        try {
            int age = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));

            if (age >= 18 && age <= 55) {
                dtTxt.setText(sdf.format(myCalendar.getTime()));

            } else {
                /*Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.valid_age), Snackbar.LENGTH_LONG);
                snackbar.show();*/

                /*age_validation.setVisibility(View.VISIBLE);
                pincode_message.setVisibility(View.GONE);
                pincode.setVisibility(View.GONE);
                pincode_layout.setVisibility(View.GONE);
                customer_details.setVisibility(View.GONE);
                customer_bank_details.setVisibility(View.GONE);*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        manager.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    public void onRequestGranted(int requestCode, String permission) {

    }

    @Override
    public void onRequestDeclined(int requestCode, String permission) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                try {
                    InputStream imageStream = getContentResolver().openInputStream(selectedUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    /*Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedUri);
                    dialog_car_image_bitmap = bitmap;
                    String encodedImage = Util.encodeImage(selectedImage);
                    if(image_flag.equalsIgnoreCase("file_img")){
                        property_file_upload_img = encodedImage;

                    }
                    else{
                        property_photo_img =  encodedImage;

                    }*/
                    //System.out.println(encodedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"ERROR Photo uploading",Toast.LENGTH_SHORT).show();
                }


            } else if (requestCode == CAMERA_CODE) {

                final Uri selectedUri = mImageCaptureUri;


                try {
                    InputStream imageStream = getContentResolver().openInputStream(selectedUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                   /* String encodedImage = Util.encodeImage(selectedImage);

                    if(image_flag.equalsIgnoreCase("file_img")){

                        property_file_upload_img = encodedImage;

                    }
                    else{
                        property_photo_img =  encodedImage;

                    }*/
                    //System.out.println(encodedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"ERROR Photo uploading",Toast.LENGTH_SHORT).show();

                }


            }else{
                if (resultCode == RESULT_OK) {
                    BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                    cash_withdrawal_bankspinner.setText(bankIINValue.getBankName());
                    // balance_enquiry_nnid = bankIINValue.getIin();
                    //cash_withdrawal_nnid = "";
                    // checkBalanceEnquiryValidation();


                }
            }


        }


    }
   /* private void uploadFile(Uri filePath) {
        //checking if file is available
       final ProgressDialog progressdl =  new ProgressDialog(AddMoneyActivity.this);
        progressdl.setMessage("Uploading...");
        progressdl.setCancelable(false);
        progressdl.show();

        try{


            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            if (filePath != null) {
                //FirebaseStorage storage = FirebaseStorage.getInstance();
                //StorageReference storageRef = storage.getReferenceFromUrl("gs://isuaepsdev.appspot.com/");
                FirebaseStorage storage = FirebaseStorage.getInstance("gs://iserveu_storage/Balance Request");
                StorageReference storageRef = storage.getReference();

                //gs://iserveu_storage/Balance Request/demoisu/itpl

                // Create a reference to "file"
                final StorageReference mountainsRef = storageRef.child());
                //adding the file to reference
                mountainsRef.putFile(filePath)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                //dismissing the progress dialog
                                progressdl.dismiss();
                                //displaying success toast
                                Toast.makeText(AddMoneyActivity.this, "File Uploaded ", Toast.LENGTH_LONG).show();

                                //creating the upload object to store uploaded image details

                                mountainsRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        //Log.d(TAG, "onSuccess: uri= "+ uri.toString());
                                        //Toast.makeText(getActivity(), "Url "+uri.toString(), Toast.LENGTH_LONG).show();
                                        Constants.VERIFIED_USER_VIDEO = uri.toString();

                                    }
                                });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                progressdl.dismiss();
                                Toast.makeText(AddMoneyActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                //displaying the upload progress
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                progressdl.setMessage("Uploaded " + ((int) progress) + "%...");
                            }
                        });
            } else {
                //display an error if no file is selected
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/
    public String getFileExtension(Uri uri) {
        ContentResolver cR = getApplicationContext().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
}
