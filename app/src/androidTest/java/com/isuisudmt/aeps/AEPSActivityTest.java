package com.isuisudmt.aeps;


import android.view.View;
import android.widget.Button;

import androidx.test.rule.ActivityTestRule;

import com.isuisudmt.R;
import com.isuisudmt.login.LoginActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class AEPSActivityTest {

    private AEPSActivity aepsActivity = null;


    @Rule
    public ActivityTestRule<AEPSActivity> activityTestRule = new ActivityTestRule<AEPSActivity>(AEPSActivity.class);

    @Before
    public void setUp() throws Exception {
        aepsActivity = activityTestRule.getActivity();
    }

    @Test
    public void TestLunch(){
        View view  = aepsActivity.findViewById(R.id.coordinatorLayout);
        assertNotNull(view);
    }

    @Test
    public void TestLunchButtonClick(){
        Button btnSubmit  = aepsActivity.findViewById(R.id.submit);
        assertNotNull(btnSubmit);
    }

    @After
    public void tearDown() throws Exception {
        aepsActivity = null;
    }

}