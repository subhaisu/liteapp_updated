package com.isuisudmt.matm;

import android.view.View;

import androidx.test.rule.ActivityTestRule;

import com.isuisudmt.R;
import com.isuisudmt.aeps.AEPSActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class MATMActivityTest {

    private MATMActivity matmActivity = null;

    @Rule
    public ActivityTestRule<MATMActivity> activityTestRule = new ActivityTestRule<MATMActivity>(MATMActivity.class);


    @Before
    public void setUp() throws Exception {
        matmActivity = activityTestRule.getActivity();
    }

    @Test
    public void TestLunch(){
        View view  = matmActivity.findViewById(R.id.coordinatorLayout);
        assertNotNull(view);
    }

    @After
    public void tearDown() throws Exception {
        matmActivity = null;
    }
}